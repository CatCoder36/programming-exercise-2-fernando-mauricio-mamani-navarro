import java.util.ArrayList;
import java.util.Scanner;

/**
 * This class manage the principal game logic.
 * @author Fernando Mauricio Mamani Navarro
 */
public class Game {
    private Board board;
    private ArrayList<Player> players;
    private Printer printer;

    /**
     * This is the constructor method.
     *
     * @see #generatePlayer()
     */
    public Game(){
        players = new ArrayList<>();
        printer = new Printer();
        board = new Board();
        generatePlayer();
    }

    /**
     * This method execute the game process.
     * In this method the game loop is executed and only ends when
     * a winning player is detected.
     *
     * @see #movePlayers()
     * @see #verifyWinner()
     * @see #printGame()
     * @see Printer
     */
    public void executeGame(){
        boolean gameFinish = false;
        int numberPlayerWinner;
        int booleanReturned = 0;
        int integerReturned = 1;

        while (!gameFinish){
            printer.printInConsole(printGame());
            movePlayers();
            gameFinish = (boolean) verifyWinner()[booleanReturned];
        }

        numberPlayerWinner = (int) verifyWinner()[integerReturned];
        printer.printInConsole(printGame());
        printer.printWinnerMessage(numberPlayerWinner);
    }

    /**
     * This method show in Console the board.
     * This method call to method drawElementsBoard of Board class.
     * Call drawPLayers method, too.
     *
     * @see #drawPlayers()
     * @see Board#drawElementsBoard()
     * @return String type, returns a String with the board elements.
     */
    private String printGame(){
        drawPlayers();
        return board.drawElementsBoard();
    }

    /**
     * This method generate a new player.
     */
    public void generatePlayer(){
        Player player = new Player(board.getLENGTH_X(), board.getLENGTH_Y());
        players.add(player);
    }

    /**
     * This method draw the player on the board.
     *
     * @see Board#drawPlayer(Player)
     */
    private void drawPlayers(){
        for(Player currentPlayer : players){
            board.drawPlayer(currentPlayer);
        }
    }

    /**
     * This method manage the players movements.
     *
     * @see Printer
     * @see Player
     * @see #insertMovement()
     * @see Board#cleanPosition(int, int)
     */
    private void movePlayers(){
        Movements movement;
        Player currentPlayer;
        for(int actualPosition = 0 ; actualPosition < players.size() ; actualPosition++){
            currentPlayer = players.get(actualPosition);
            printer.printInsertMovement(actualPosition+1 , currentPlayer.getIcon());
            movement = insertMovement();
            if(checkValidMovement(movement,currentPlayer)){
                board.cleanPosition(currentPlayer.getActualPositionX(), currentPlayer.getActualPositionY());
                currentPlayer.movePlayer(movement);
            }
        }
    }

    /**
     * This method receives the movement Player
     *
     * @see Printer
     * @return Movements type, returns the movements type.
     */
    private Movements insertMovement(){
        Scanner scanner = new Scanner(System.in);
        String dataInserted;
        Movements movement = null;
        while(movement == null){
            dataInserted =  scanner.nextLine().toUpperCase();
            switch (dataInserted){
                case "UP":
                    movement = Movements.UP;
                    break;
                case "DOWN":
                    movement = Movements.DOWN;
                    break;
                case "LEFT":
                    movement = Movements.LEFT;
                    break;
                case "RIGHT":
                    movement = Movements.RIGHT;
                    break;
                default:
                    printer.printErrorMessage();
            }
        }
        return movement;
    }

    /**
     * This method verify if a movement is valid.
     *
     * @see Board#checkAvailableCell(int, int)
     * @param movement Movement type, receives movement type.
     * @param player Player type, receives a player.
     * @return Boolean type, returns if  a movement is valid.
     *
     */
    private boolean checkValidMovement(Movements movement, Player player){
        boolean isValid ;
        switch (movement){
            case UP:
                isValid = board.checkAvailableCell(player.getActualPositionX(), player.getActualPositionY()-1);
                break;
            case DOWN:
                isValid = board.checkAvailableCell(player.getActualPositionX(), player.getActualPositionY()+1);
                break;
            case LEFT:
                isValid = board.checkAvailableCell(player.getActualPositionX()-1, player.getActualPositionY());
                break;
            case RIGHT:
                isValid = board.checkAvailableCell(player.getActualPositionX()+1, player.getActualPositionY());
                break;
            default:
                isValid = false;
        }
        return isValid;
    }

    /**
     * This method verify when a player is winner.
     * This method go to players list and verify if the player position
     * is same with the winner position.
     *
     * @see Board#verifyWinner(Player)
     * @return Object[] type, returns the tuple with the comparing results.
     */
    private Object[] verifyWinner(){
        Object [] verify = new Object[2];
        for(int currentPlayer = 0; currentPlayer < players.size(); currentPlayer++){
            if(board.verifyWinner(players.get(currentPlayer))) {
                verify[0] = true;
                verify[1] = currentPlayer;
            }
            else {
                verify[0] = false;
            }
        }
        return verify;
    }
}
