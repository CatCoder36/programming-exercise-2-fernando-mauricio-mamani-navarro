class Node3<T> {
    private T data;
    private Node3<T> next;

    public Node3(T data) {
        this.data = data;
    }

    public T getData() {
        return data;
    }

    public Node3<T> getNext() {
        return next;
    }

    public void setNext(Node3<T> node) {
        next = node;
    }

    public String toString() {
        return "{data=" + data + ", sig->" + next + "}";
    }
}
