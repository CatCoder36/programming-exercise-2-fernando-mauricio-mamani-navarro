import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class JULinkedList<T> implements List<T> {
    private Nodo<T> root;
    private int quantityElements = 0;

    @Override
    public int size() {
        return quantityElements;
    }

    @Override
    public boolean isEmpty() {
        boolean isEmpty =  root == null ? true : false;
        return isEmpty;
    }

    @Override
    public boolean contains(Object o) {
        boolean elementInTheList = false;
        for(Nodo<T> currentNode = root; currentNode != null ; currentNode = currentNode.getNext() ){
            if(currentNode.getData().equals(o)){
                elementInTheList = true;
            }
        }
        return elementInTheList;
    }

    @Override
    public Iterator<T> iterator() {
        Iterator<T> counter = new Iterator<T>(){

            int counterElements = 0;
            Nodo auxiliaryNode = root;

            @Override
            public boolean hasNext() {
                return counterElements < size();
            }

            @Override
            public T next() {
                T valueReturned = null;
                if(counterElements != 0){
                    auxiliaryNode = auxiliaryNode.getNext();
                }
                valueReturned = (T) auxiliaryNode.getData();
                counterElements++;
                return valueReturned;
            }
        };
        return counter;
    }

    @Override
    public Object[] toArray() {
        //I could not implement it
        return new Object[0];
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        //I could not implement it
        return null;
    }

    @Override
    public boolean add(T t) {
        if(root == null){
            root = new Nodo<T>(t,null);
        }else {
            root = new Nodo<T>(t, root);
        }
        quantityElements++;

        return true;
    }

    @Override
    public boolean remove(Object data) {
        boolean stateOperation = false;
        Nodo<T> previous = root;
        for (Nodo<T> actual = root; actual != null; previous = actual, actual = actual.getNext()) {
            if (actual.getData().equals(data)){
                if (root == actual) {
                    root = actual.getNext();
                }
                else {
                    previous.setNext(actual.getNext());
                }
                stateOperation = true;
                quantityElements--;
            }
        }
        return stateOperation;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        //I could not implement it
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        //I could not implement it
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        //I could not implement it
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        //I could not implement it
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        //I could not implement it
        return false;
    }

    @Override
    public void clear() {
        root = null;
        quantityElements = 0;
    }

    @Override
    public T get(int index) {
        T valueReturned = null;
        Nodo<T> currentNode = root;
        for(int counter= 0; counter < quantityElements ; counter++){
            if(counter == index){
                valueReturned = currentNode.getData();
            }
            currentNode = currentNode.getNext();
        }
        return valueReturned;
    }

    @Override
    public T set(int index, T element) {
        T elementPrevious = null;
        Nodo<T> currentNode = root;
        for(int counter= 0; counter < quantityElements ; counter++, currentNode = currentNode.getNext()){
            if(counter == index){
                elementPrevious = currentNode.getData();
                currentNode.setData(element);
            }
        }
        return elementPrevious;
    }

    @Override
    public void add(int index, T element) {
        //I could not implement it
    }

    @Override
    public T remove(int index) {
        T elementRemoved = null;
        Nodo<T> currentNode = root;
        Nodo<T> previousNode = root;
        if(index <= quantityElements){
            int firstPosition = 0;
            for(int counter = 0; counter < quantityElements ; counter++, currentNode = currentNode.getNext()){
                if(counter == index){
                    if(counter == firstPosition){
                        root = currentNode.getNext();
                    }
                    else {
                        elementRemoved = currentNode.getData();
                        previousNode.setNext(currentNode.getNext());
                    }
                    quantityElements--;
                }
                previousNode = currentNode;
            }
        }
        return elementRemoved;
    }

    @Override
    public int indexOf(Object o) {
        int indexElement = -1;
        Nodo<T> currentNode = root;
        for(int counter = 0; counter < quantityElements ; counter++, currentNode = currentNode.getNext()){
            if(currentNode.getData().equals(o)){
                indexElement = counter;
                counter = quantityElements;
            }
        }
        return indexElement;
    }

    @Override
    public int lastIndexOf(Object o) {
        int indexElement = -1;
        Nodo<T> currentNode = root;
        for(int counter = 0; counter < quantityElements ; counter++, currentNode = currentNode.getNext()){
            if(currentNode.getData().equals(o)){
                indexElement = counter;
            }
        }
        return indexElement;
    }

    @Override
    public ListIterator<T> listIterator() {
        //I could not implement it
        return null;
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        //I could not implement it
        return null;
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        //I could not implement it
        return null;
    }

    public String toString() {
        return "Number elements ("+quantityElements+")raiz=" + root;
    }

    public String getElements(){
        String elements = "";
       for (Nodo<T> currentElement = root; currentElement != null ; currentElement = currentElement.getNext()){
           elements += currentElement.getData()+", ";
       }
       return elements;
    }
}
