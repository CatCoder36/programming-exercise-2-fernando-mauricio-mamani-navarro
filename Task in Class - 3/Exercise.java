public class Exercise {
    Node5 head;
    Node5 tail;

    public Exercise(){
        head = null;
        tail = null;
    }

    public void insertElement(int data){
        if(head == null){
            head = new Node5(data, head);
            tail = head;
        }
        else {
            head = new Node5(data, head);
        }
    }

    public int deque(){
        int valueRemoved = tail.getData();
        boolean removed = false;
        for(Node5 currentNode = head; head.getNext() != null ; currentNode = currentNode.getNext() ){
            if(currentNode.getNext() == tail){
                currentNode.setNext(null);
                tail = currentNode;
                removed = true;
                break;
            }
        }
        if(head.getNext() == null && !removed){
            head = null;
            tail = null;
        }

        return valueRemoved;
    }

    @Override
    public String toString() {
        return head.toString();
    }

    public int getHead(){
        return head.getData();
    }

    public int getTail(){
        return tail.getData();
    }
}
