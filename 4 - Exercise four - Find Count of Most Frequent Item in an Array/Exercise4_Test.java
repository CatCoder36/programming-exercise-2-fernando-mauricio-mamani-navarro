import org.junit.Assert;
import org.junit.Test;

public class Exercise4_Test {
    @Test
    public void testSearchNumberFrequentlyElement(){
        ArrayAnalyzer arrayAnalyzer = new ArrayAnalyzer();
        int[] arrayTest = new int[]{4,5,9,1,1,5,6,6,4,2,4,5,8,5,5,1,5};
        int valueExpected = arrayAnalyzer.searchNumberFrequentlyElement(arrayTest);
        Assert.assertEquals(6,valueExpected);

        int[] arrayTest2 = new int[]{1,9,7,5,2,3,6,1,1,1,4,4,7,7,1,1,1};
        int valueExpected2 = arrayAnalyzer.searchNumberFrequentlyElement(arrayTest2);
        Assert.assertEquals(7,valueExpected2);

        int[] arrayTest3 = new int[0];
        int valueExpected3 = arrayAnalyzer.searchNumberFrequentlyElement(arrayTest3);
        Assert.assertEquals(0,valueExpected3);
    }
}
