import org.junit.Assert;
import org.junit.Test;

import java.util.Iterator;

public class TestJUArrayList {

    @Test
    public void testSize(){
        JUArrayList juArrayList = new JUArrayList();
        juArrayList.add(5);
        juArrayList.add(6);
        juArrayList.add(7);
        int valueExpected = juArrayList.size();
        Assert.assertEquals(3,valueExpected);

        juArrayList.add(10);
        juArrayList.add(12);
        valueExpected = juArrayList.size();
        Assert.assertEquals(5,valueExpected);

        juArrayList.remove(0);
        juArrayList.remove(1);
        juArrayList.remove(2);
        valueExpected = juArrayList.size();
        Assert.assertEquals(2,valueExpected);
    }
    @Test
    public void testIsEmpty() {
        JUArrayList arrayTest1 = new JUArrayList();
        boolean valueObtained = arrayTest1.isEmpty();
        Assert.assertEquals(true, valueObtained);

        arrayTest1.add(5);
        valueObtained = arrayTest1.isEmpty();
        Assert.assertEquals(false,valueObtained);

        arrayTest1.remove(0);
        valueObtained = arrayTest1.isEmpty();
        Assert.assertEquals(true,valueObtained);
    }

    @Test
    public void testIterator(){
        JUArrayList juArrayList = new JUArrayList();
        juArrayList.add(6);
        juArrayList.add(5);
        juArrayList.add(7);
        Iterable<Integer> hola = new Iterable<Integer>() {
            @Override
            public Iterator<Integer> iterator() {
                return juArrayList.iterator();
            }
        };
        Integer[] arrayTest = new Integer[3];
        int index = 0;
        for (Integer currentElement : juArrayList){
            arrayTest[index] = currentElement;
            index++;
        }

        Assert.assertEquals(juArrayList.getArray(), arrayTest);
    }
    @Test
    public void testAdd(){
        JUArrayList array = new JUArrayList();
        array.add(5);
        array.add(4);
        array.add(6);
        array.add(9);
        array.add(10);
        array.add(78);
        array.add(48);
        array.add(74);
        int valueExpected = array.size();
        Assert.assertEquals(8,valueExpected);

        Integer elementToRemove = 5;
        array.remove(elementToRemove);
        array.add(100);
        array.add(101);
        valueExpected = array.size();
        Assert.assertEquals(9,valueExpected);

        array.add(200);
        array.add(201);
        valueExpected = array.size();
        Assert.assertEquals(11,valueExpected);
    }

    @Test
    public void testRemove(){
        JUArrayList juArrayList = new JUArrayList();
        juArrayList.add(5);
        juArrayList.add(6);
        juArrayList.add(7);
        juArrayList.add(8);
        juArrayList.add(9);
        juArrayList.add(10);
        juArrayList.add(11);
        juArrayList.add(12);

        Integer elementRemoved = 5;
        Integer elementRemoved2 = 6;
        Integer elementRemoved3 = 7;
        Integer elementRemoved4 = 8;
        juArrayList.remove(elementRemoved);
        juArrayList.remove(elementRemoved2);
        juArrayList.remove(elementRemoved3);
        juArrayList.remove(elementRemoved4);

        Integer[] arrayTest = new Integer[]{9,10,11,12};

        Assert.assertEquals(arrayTest, juArrayList.getArray());

        Integer[] arrayTest2 = new Integer[]{9,10,12};
        Integer elementRemoved5 = 11;
        juArrayList.remove(elementRemoved5);

        Assert.assertEquals(arrayTest2,juArrayList.getArray());
    }

    @Test
    public void testClear(){
        JUArrayList juArrayList = new JUArrayList();
        juArrayList.add(5);
        juArrayList.add(6);
        juArrayList.add(7);
        juArrayList.add(8);
        juArrayList.add(9);
        juArrayList.add(10);
        juArrayList.add(11);
        juArrayList.add(12);

        int valueExpected = juArrayList.size();
        Assert.assertEquals(8,valueExpected);

        juArrayList.clear();
        valueExpected = juArrayList.size();
        Assert.assertEquals(0, valueExpected);
    }

    @Test
    public void testGet(){
        JUArrayList juArrayList = new JUArrayList();
        juArrayList.add(5);
        juArrayList.add(6);
        juArrayList.add(7);
        juArrayList.add(8);
        juArrayList.add(9);
        juArrayList.add(10);
        juArrayList.add(11);
        juArrayList.add(12);

        int valueExpected = juArrayList.get(2);
        Assert.assertEquals(7, valueExpected);

        valueExpected = juArrayList.get(3);
        Assert.assertEquals(8,valueExpected);

        valueExpected = juArrayList.get(4);
        Assert.assertEquals(9,valueExpected);

        valueExpected = juArrayList.get(5);
        Assert.assertEquals(10,valueExpected);

        valueExpected = juArrayList.get(6);
        Assert.assertEquals(11,valueExpected);

        valueExpected = juArrayList.get(7);
        Assert.assertEquals(12,valueExpected);

    }

    @Test
    public void testRemoveForIndex(){
        JUArrayList juArrayList = new JUArrayList();
        juArrayList.add(5);
        juArrayList.add(6);
        juArrayList.add(7);
        juArrayList.add(8);
        juArrayList.add(9);
        juArrayList.add(10);
        juArrayList.add(11);
        juArrayList.add(12);

        juArrayList.remove(1);
        Integer[] arrayTest = new Integer[]{5,7,8,9,10,11,12};

        Assert.assertEquals(arrayTest,juArrayList.getArray());

        juArrayList.remove(2);
        Integer[] arrayTest3 = new Integer[]{5,7,9,10,11,12};

        Assert.assertEquals(arrayTest3,juArrayList.getArray());

        juArrayList.remove(5);
        Integer[] arrayTest4 = new Integer[]{5,7,9,10,11};

        Assert.assertEquals(arrayTest4,juArrayList.getArray());

        juArrayList.remove(3);
        Integer[] arrayTest5 = new Integer[]{5,7,9,11};

        Assert.assertEquals(arrayTest5,juArrayList.getArray());

        juArrayList.remove(1);
        Integer[] arrayTest6 = new Integer[]{5,9,11};

        Assert.assertEquals(arrayTest6,juArrayList.getArray());
    }

}
