package Exceptions;

/**
 * This class is an Exception.
 * This exception is generated when a number exceeds the allowed limit.
 */
public class NumberMajorToTheLimitException extends CalcException{
    /**
     * This is the constructor method.
     *
     * @param invalidNumber, String type, receives the number invalid in String format.
     */
    public NumberMajorToTheLimitException(String invalidNumber){
        super(invalidNumber);
    }

    /**
     * This method returns the message of the exception.
     *
     * @return String type, returns the message of the exception.
     */
    @Override
    public String getExceptionMessage(){
        return "Exception : The inserted number exceeds the allowed limit. : "+"<< "+super.getCharacterInvalid()+" >>";
    }
}
