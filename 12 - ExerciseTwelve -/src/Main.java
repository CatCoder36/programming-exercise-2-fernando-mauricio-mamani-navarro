
public class Main {
    public static void main(String[] args) {
        // 1.- sacar screenshot de la salida del metodo
        // 2.- sacar screenshot, del callStack (Debug) con un breakpoint en LlamadasEncadenadas.doFour
        //LlamadasEncadenadas.doOne();

        // descomentar la llamda a: Infinito.whileTrue()
        // 1.- sacar screenshot de la exception
        // 2.- a continuacion describir el problema:
        // Descripcion: Es un fallo de segmentación, al existir recursividad infinita la pila(memoria donde
        //              se almacena la direccion de retorno) se llena hasta desbordarse hasta llegar a un punto
        //              de la memoria donde el sistema operativo no le deja acceder provocando este error de
        //              segmentación.
        //
        // 3.- volver a comentar la llmanda a: Infinito.whileTrue()
        // Infinito.whileTrue();

        // descomentar la llamda a: Contador.contarHasta(), pasnado un numero entero positivo
        // 1.- sacar screenshot de la salida del metodo
        // 2.- a continuacion describir como funciona este metodo:
        // Descripcion: El metodo contadorHasta(int contador) recibe un parametro de tipo Integer, el valor original de este
        //              valor se imprime en consola y luego pasa por una condicional analizando si este número es mayor que cero,
        //              a continuación se llama nuevamente a este metodo pasandole por parametro el mismo número pero disminuido un valor(-1).
        //              De esta manera se ejecuta el mismo metodo y el mismo prodecimiento hasta que de la condicional de false.
        //              Esta forma de recursividad funciona de igual manera que un bucle while.
        Contador.contarHasta(10);

        //System.out.println(RecursividadvsIteracion.factorialIter(5));
        //System.out.println(RecursividadvsIteracion.factorialRecu(5));
    }
}
