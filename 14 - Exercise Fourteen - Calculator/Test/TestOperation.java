import Operators.FactorialMath;
import Operators.MultiplicationMath;
import Operators.PowerMath;
import Operators.SumMath;
import Stack.*;
import org.junit.Assert;
import org.junit.Test;
public class TestOperation {
    @Test
    public void testExecuteFirstOperationFound(){
        SumMath operation = new SumMath();
        Stack<Character> stackWithOperation = new Stack<Character>();
        stackWithOperation.push('2');
        stackWithOperation.push('+');
        stackWithOperation.push('2');
        stackWithOperation.push('+');
        stackWithOperation.push('6');
        Stack<Character> result = operation.executeFirstOperationFounded(stackWithOperation);
        String expectedResult = "8, +, 2, ";
        String currentResult = result.getElements();

        Assert.assertEquals(expectedResult, currentResult);

        Stack<Character> stack = new Stack<Character>();

        stack.push('7');
        stack.push('+');
        stack.push('5');
        stack.push('7');
        stack.push('+');
        stack.push('1');
        result = operation.executeFirstOperationFounded(stack);
        expectedResult = "7, 6, +, 7, ";
        currentResult = result.getElements();
        Assert.assertEquals(expectedResult, currentResult);

        MultiplicationMath multiplicationMath = new MultiplicationMath();
        Stack<Character> stack2 = new Stack<Character>();

        stack2.push('4');
        stack2.push('*');
        stack2.push('3');
        stack2.push('*');
        stack2.push('2');
        result = multiplicationMath.executeFirstOperationFounded(stack2);
        expectedResult = "6, *, 4, ";
        currentResult = result.getElements();
        Assert.assertEquals(expectedResult, currentResult);

        PowerMath powerMath = new PowerMath();
        Stack<Character> stack3 = new Stack<Character>();
        stack3.push('3');
        stack3.push('^');
        stack3.push('3');
        stack3.push('+');
        stack3.push('2');
        stack3.push('^');
        stack3.push('3');
        result = powerMath.executeFirstOperationFounded(stack3);
        expectedResult = "9, +, 3, ^, 3, ";
        currentResult = result.getElements();
        Assert.assertEquals(expectedResult, currentResult);

        FactorialMath factorialMath = new FactorialMath();
        Stack<Character> stack4 = new Stack<Character>();
        stack4.push('!');
        stack4.push('4');
        stack4.push('+');
        stack4.push('!');
        stack4.push('5');
        result = factorialMath.executeFirstOperationFounded(stack4);
        expectedResult = "1, 2, 0, +, 4, !, ";
        currentResult = result.getElements();
        Assert.assertEquals(expectedResult, currentResult);

    }

    @Test
    public void testExecuteOperation(){
        FactorialMath factorialMath = new FactorialMath();
        Stack<Character> stackWithResult = factorialMath.getIntegerFactorial(5);
        int result = StackTools.convertStackCharacterToInt(stackWithResult);
        int expectedResult = 120;
        Assert.assertEquals(expectedResult, result);

        stackWithResult = factorialMath.getIntegerFactorial(4);
        result = StackTools.convertStackCharacterToInt(stackWithResult);
        expectedResult = 24;

        Assert.assertEquals(expectedResult, result);

        PowerMath powerMath = new PowerMath();
        result = StackTools.convertStackCharacterToInt(powerMath.elevateIntegerToPower(2,4));
        expectedResult = 16;

        Assert.assertEquals(expectedResult, result);

        MultiplicationMath multiplicationMath = new MultiplicationMath();
        result = StackTools.convertStackCharacterToInt(multiplicationMath.multiplyIntegers(4,5));
        expectedResult = 20;

        Assert.assertEquals(expectedResult, result);

        SumMath sumMath = new SumMath();
        result = StackTools.convertStackCharacterToInt(sumMath.sumIntegers(4,487));
        expectedResult = 491;

        Assert.assertEquals(expectedResult, result);

    }
}
