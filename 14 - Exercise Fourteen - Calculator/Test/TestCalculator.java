import Calculator.*;
import Operators.Operation;
import org.junit.Assert;
import org.junit.Test;
public class TestCalculator {
    @Test
    public void testSum(){
        Calculator calculator = new Calculator();
        int currentResult = calculator.calculate("1+1");
        int resultExpected = 2;
        Assert.assertEquals(resultExpected, currentResult);

        currentResult = calculator.calculate("44+2+6+3");
        resultExpected = 55;
        Assert.assertEquals(resultExpected, currentResult);

        currentResult = calculator.calculate("2147483647+0");
        resultExpected = 2147483647;
        Assert.assertEquals(resultExpected, currentResult);

        currentResult = calculator.calculate("60+500+63+3");
        resultExpected = 626;
        Assert.assertEquals(resultExpected, currentResult);
    }
    @Test
    public void testMultiplication(){
        Calculator calculator = new Calculator();
        int currentResult = calculator.calculate("5*5");
        int expectedResult = 25;
        Assert.assertEquals(expectedResult, currentResult);

        currentResult = calculator.calculate("9*8*7");
        expectedResult = 504;
        Assert.assertEquals(expectedResult, currentResult);

        currentResult = calculator.calculate("29*20*0");
        expectedResult = 0;
        Assert.assertEquals(expectedResult, currentResult);
    }
    @Test
    public void testPower(){
        Calculator calculator = new Calculator();
        int currentResult = calculator.calculate("5^2");
        int expectedResult = 25;
        Assert.assertEquals(expectedResult, currentResult);

        currentResult = calculator.calculate("10^3");
        expectedResult = 1000;
        Assert.assertEquals(expectedResult, currentResult);

        currentResult = calculator.calculate("3^3");
        expectedResult = 27;
        Assert.assertEquals(expectedResult, currentResult);

        currentResult = calculator.calculate("64^1");
        expectedResult = 64;
        Assert.assertEquals(expectedResult, currentResult);
    }

    @Test
    public void testFactorial(){
        Calculator calculator = new Calculator();
        int currentResult = calculator.calculate("5!");
        int expectedResult = 120;
        Assert.assertEquals(expectedResult, currentResult);

        currentResult = calculator.calculate("11!");
        expectedResult = 39916800;
        Assert.assertEquals(expectedResult, currentResult);

        currentResult = calculator.calculate("8!");
        expectedResult = 40320;
        Assert.assertEquals(expectedResult, currentResult);
    }

    @Test
    public void operationCombined(){
        Calculator calculator = new Calculator();
        int currentResult = calculator.calculate("5+8*2");
        int expectedResult = 26;
        Assert.assertEquals(expectedResult, currentResult);

        currentResult = calculator.calculate("2^3+2");
        expectedResult = 10;
        Assert.assertEquals(expectedResult, currentResult);

        currentResult = calculator.calculate("5!+10");
        expectedResult = 130;
        Assert.assertEquals(expectedResult, currentResult);

        currentResult = calculator.calculate("3!^2");
        expectedResult = 36;
        Assert.assertEquals(expectedResult, currentResult);

        currentResult = calculator.calculate("3^3!+5*2");
        expectedResult = 1468;
        Assert.assertEquals(expectedResult, currentResult);

        currentResult = calculator.calculate("4+6^6*2!");
        expectedResult = 93320;
        Assert.assertEquals(expectedResult, currentResult);

        currentResult = calculator.calculate("8!+9^2*2+4+156+87+6*2");
        expectedResult = 20604510;
        Assert.assertEquals(expectedResult, currentResult);
    }

    @Test
    public void testOperationsWithParenthesis(){
        Calculator calculator = new Calculator();
        int currentResult = calculator.calculate("(1+2)+3");
        int expectedResult = 6;
        Assert.assertEquals(expectedResult, currentResult);

        currentResult = calculator.calculate("2*(2*5)");
        expectedResult = 20;
        Assert.assertEquals(expectedResult, currentResult);

        currentResult = calculator.calculate("(2^3)+2");
        expectedResult = 10;
        Assert.assertEquals(expectedResult, currentResult);

        currentResult = calculator.calculate("2^(3+2)");
        expectedResult =  32;
        Assert.assertEquals(expectedResult, currentResult);

        currentResult = calculator.calculate("(1+2)*5");
        expectedResult = 15;
        Assert.assertEquals(expectedResult, currentResult);

        currentResult = calculator.calculate("3^3!+(5*2)");
        expectedResult = 739;
        Assert.assertEquals(expectedResult, currentResult);

        currentResult = calculator.calculate("4^(2*3)");
        expectedResult = 4096;
        Assert.assertEquals(expectedResult, currentResult);
    }
}
