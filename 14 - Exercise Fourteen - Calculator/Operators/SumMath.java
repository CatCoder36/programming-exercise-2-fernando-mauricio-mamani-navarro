package Operators;

import Exceptions.NumberMajorToTheLimitException;
import Stack.*;
public class SumMath extends Operation {

    public SumMath() {
        super('+');
    }

    public Stack<Character> sumIntegers(int numberOne, int numberTwo){
        Stack<Character> resultStack;
        int result = numberOne + numberTwo;
        if(result < 0){
            super.calcException = new NumberMajorToTheLimitException(numberOne+"+"+numberTwo);
        }
        resultStack = StackTools.copyIntegerToStack(result);
        return resultStack;
    }

    @Override
    public Stack<Character> executeOperation(int firstNumber, int secondNumber) {
        return sumIntegers(firstNumber, secondNumber);
    }



}
