package Operators;
import Stack.*;
public class PowerMath extends Operation {
    public PowerMath() {
        super('^');
    }
    @Override
    protected Stack<Character> executeOperation(int firstNumber, int secondNumber){
        return elevateIntegerToPower(firstNumber,secondNumber);
    }

    public Stack<Character> elevateIntegerToPower(int number, int power){
        Stack<Character> resultStack;
        int result = number;
        for (int index = 1; index < power; index++){
            result = result*number;
        }
        resultStack = StackTools.copyIntegerToStack(result);
        return resultStack;
    }
}
