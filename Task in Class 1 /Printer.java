/**
 * This method manage the prints the game.
 * @author Fernando Mauricio Mamani Navarro
 */
public class Printer {
    public void printInsertMovement(int numberPlayer , String icon){
        System.out.print("Player "+numberPlayer+"  <"+icon+"> : Ok, insert new movement(UP, DOWN, LEFT, RIGHT)  : ");
    }
    public void printInConsole(String message){
        System.out.println(message);
    }
    public void printErrorMessage(){
        System.out.println("Error: please insert validate movement");
    }

    public void printWinnerMessage(int numberPlayer){
        System.out.println("Congratulation Player "+(numberPlayer+1)+" you winn");
    }

}
