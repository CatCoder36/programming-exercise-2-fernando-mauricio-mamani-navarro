class Node{
    public int data;
    public Node next = null;

    public static int getNth(Node node, int index) throws Exception{
            Node currentNode = node;
            for(int currentPosition = 1; currentPosition <= index ; currentPosition++){
                currentNode = currentNode.getNext();
            }
        return currentNode.data;
    }

    public Node getNext(){
        return next;
    }


}