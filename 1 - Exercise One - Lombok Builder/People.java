/**
 * This is the class People, abstracts the main attributes of a person.
 */
public class People{
    private int age;
    private String name;
    private String lastName;
    private final String GREET="hello";

    /**
     * This is the constructor method
     *
     * @param age int type, receive the student age
     * @param name String type, receive the student name
     * @param lastName String type, receive the student lastName
     */
    public People(int age, String name, String lastName) {
        this.age = age;
        this.name = name;
        this.lastName = lastName;
    }

    /**
     * This method return to greet.
     *
     * @return String type, return a String to greet.
     */
    public String greet(){
        return GREET+" my name is "+name;
    }

    /**
     * This method return to age.
     *
     * @return int type, return the student age.
     */
    public int getAge() {
        return age;
    }
    /**
     * This method return name.
     *
     * @return String type, return the student name.
     */
    public String getName() {
        return name;
    }
    /**
     * This method return to last name.
     *
     * @return String type, return the student lastName
     */
    public String getLastName() {
        return lastName;
    }
    /**
     * This method return to initial greet.
     *
     * @return String type, return a String to initial greet.
     */
    public String getGREET() {
        return GREET;
    }
}