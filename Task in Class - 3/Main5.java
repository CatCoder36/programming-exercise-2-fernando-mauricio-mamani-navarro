public class Main5 {
    public static void main(String[] args) {
       Exercise tailOne = new Exercise();
       tailOne.insertElement(5);
       tailOne.insertElement(3);
       tailOne.insertElement(1);

        System.out.println(tailOne);

        Exercise tailTwo = new Exercise();

        tailTwo.insertElement(6);
        tailTwo.insertElement(4);
        tailTwo.insertElement(2);

        System.out.println(tailTwo);

        Exercise tailsMerged = new Exercise();
        boolean controller = true;
        int index = 0;
        while (controller) {
            try {
                if(index %2 != 0){
                    tailsMerged.insertElement(tailOne.deque());
                }
                else {
                    tailsMerged.insertElement(tailTwo.deque());
                }
                index++;
            }catch (Exception e){
                System.out.println(tailsMerged);
                controller= false;
            }
        }




    }


}
