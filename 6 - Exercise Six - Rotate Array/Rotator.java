/**
 * This class rotate an Array.
 * This class handles the logic of rearranging the elements of an array.
 *
 * @author Fernando Mauricio Mamani Navarro - Paralelo : A
 */
public class Rotator {
    /**
     * This method rotates an Array as many times as it is inserted.
     * This method directs the methods to move the elements to the right or
     * left as many times as it is passed by parameter
     *
     * @param arrayAnalyzed Object[] type, receives the array to analyze.
     * @param quantityRotate integer type, receives the quantity rotate.
     * @return Object[] type, returns the array sorted.
     */
    public Object[] rotateArray(Object[] arrayAnalyzed, int quantityRotate){
        TypeRotation typeRotation = analyzeTypeRotation(quantityRotate);
        for (int numberRotations = 0; numberRotations < Math.abs(quantityRotate); numberRotations++){
            switch (typeRotation){
                case ROTATION_TO_RIGHT:
                    arrayAnalyzed = rotateToRight(arrayAnalyzed);
                    break;
                case ROTATION_TO_LEFT:
                    arrayAnalyzed = rotateToLeft(arrayAnalyzed);
            }
        }
        return arrayAnalyzed;
    }

    /**
     * This method rotate the array elements to right.
     * This method using auxiliary variables moves all elements of an array
     * one space to the right.
     *
     * @param arrayRotate Object[] type, receives the array to rotate.
     * @return Object[] type. returns the array rotated
     */
    private Object[] rotateToRight(Object[] arrayRotate){

        int nextPosition ;
        int finalPosition = arrayRotate.length - 1;
        int firstPosition = 0;
        Object previousValue = arrayRotate[firstPosition];
        Object auxiliaryValue = null;
        for(int currentPosition = 0; currentPosition < arrayRotate.length; currentPosition++){
            nextPosition = currentPosition+1;
            auxiliaryValue = currentPosition != finalPosition? arrayRotate[nextPosition] : previousValue;
            if(currentPosition == finalPosition){
                arrayRotate[firstPosition] = auxiliaryValue;
            }
            else {
               arrayRotate[nextPosition] = previousValue;
               previousValue = auxiliaryValue;
            }
        }
        return arrayRotate;
    }
    /**
     * This method rotate the array elements to left.
     * This method using auxiliary variables moves all elements of an array
     * one space to the left.
     *
     * @param arrayRotate Object[] type, receives the array to rotate.
     * @return Object[] type. returns the array rotated
     */
    private Object[] rotateToLeft(Object[] arrayRotate){
        int previousPosition;
        int firstPosition = 0;
        Object previousValue = arrayRotate[firstPosition];
        Object auxiliaryValue = null;

        for(int currentPosition = arrayRotate.length; currentPosition > 0 ; currentPosition--){
            previousPosition = currentPosition-1;
            auxiliaryValue = currentPosition != firstPosition? arrayRotate[previousPosition] : previousValue;
            arrayRotate[previousPosition] = previousValue;
            previousValue = auxiliaryValue;

        }
        return arrayRotate;
    }

    /**
     * This method analyzes which side to rotate to.
     * This method receives the number of rotations from the main method and verifies
     * what type of rotation should be done.
     *
     * @param numberAnalyzed, int type, receives the number to analyze
     * @return TypeRotation type, returns the rotation type.
     */
    private TypeRotation analyzeTypeRotation(int numberAnalyzed){
        TypeRotation typeReturned;
        if(numberAnalyzed > 0){
            typeReturned = TypeRotation.ROTATION_TO_RIGHT;
        } else if (numberAnalyzed < 0) {
            typeReturned = TypeRotation.ROTATION_TO_LEFT;
        }
        else {
            typeReturned = null;
        }
        return typeReturned;
    }
}
