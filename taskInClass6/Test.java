import org.junit.Assert;

public class Test {
    @org.junit.Test
    public void testTail(){
        TailExercise<Integer> tailExercise = new TailExercise<>();
        tailExercise.insertElement(5);
        tailExercise.insertElement(4);
        tailExercise.insertElement(3);
        tailExercise.insertElement(2);
        tailExercise.insertElement(1);
        System.out.println(tailExercise);

        String expected = "1, 2, 3, 4, 5, ";
        String actual = tailExercise.getElements();

        Assert.assertEquals(expected,actual);

        tailExercise.insertElement(5);
         expected = "1, 2, 3, 4, 5, ";
         actual = tailExercise.getElements();

        Assert.assertEquals(expected,actual);

        tailExercise.insertElement(6);
        expected = "6, 1, 2, 3, 4, 5, ";
        actual = tailExercise.getElements();

        Assert.assertEquals(expected,actual);
    }
}
