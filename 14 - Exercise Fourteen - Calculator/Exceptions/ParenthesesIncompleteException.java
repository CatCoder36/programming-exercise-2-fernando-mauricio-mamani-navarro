package Exceptions;

/**
  * This class is an Exception.
 *  This exception is generated when the parentheses are not complete,
 *  when they are not closed or when there is an extra parenthesis.
 */
public class ParenthesesIncompleteException extends CalcException{

    /**
     * This is the constructor method
     *
     * @param invalidElement, String type, receives the element invalid
     */
    public ParenthesesIncompleteException(String invalidElement){
        super(invalidElement);
    }

    /**
     * This method returns the character that generated the exception.
     *
     * @return String type, returns the character that generated the exception.
     */
    @Override
    public String getExceptionMessage(){
        return "Exception: Incomplete parentheses, excess parentheses : "+"<< "+super.getCharacterInvalid()+" >>";
    }
}
