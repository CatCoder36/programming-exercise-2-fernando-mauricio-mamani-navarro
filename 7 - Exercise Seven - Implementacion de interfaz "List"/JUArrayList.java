import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class JUArrayList implements List<Integer> {
    private Integer[] array;
    private int indexElements;
    public JUArrayList(){
        array = new Integer[0];
        indexElements = 0;
    }
    @Override
    public int size() {
        return array.length;
    }

    @Override
    public boolean isEmpty() {
        return !(array.length>0);
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public Iterator<Integer> iterator() {
        Iterator<Integer> counter = new Iterator<Integer>(){
            int index = 0;
            @Override
            public boolean hasNext() {

                return index < array.length;
            }

            @Override
            public Integer next() {
                int valueReturned  = array[index];
                index++;
                return valueReturned;
            }
        };
        return counter;
    }

    @Override
    public Object[] toArray() {
        //I could not implant it
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] a) {
        //I could not implant it
        return null;
    }

    @Override
    public boolean add(Integer integer) {
        Integer[] provisionalArray = new Integer[array.length+1];
        for(int currentElement = 0; currentElement < array.length ; currentElement++){
            provisionalArray[currentElement] = array[currentElement];
        }
        provisionalArray[indexElements] = integer;
        array = provisionalArray;
        indexElements++;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        boolean statusOperation;
        try {
            Integer[] provisionalArray = new Integer[array.length-1];
            Integer valueToRemove = (Integer) o;
            int indexProvisionalArray = 0;
            for(int currentPosition = 0; currentPosition < array.length ; currentPosition++){
                if(valueToRemove != array[currentPosition]){
                    provisionalArray[indexProvisionalArray] = array[currentPosition];
                    indexProvisionalArray++;
                }
            }
            array = provisionalArray;
            indexElements--;
            statusOperation = true;
        }catch (Exception e){
            statusOperation = false;
        }
        return statusOperation;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        //I could not implant it
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends Integer> c) {
        //I could not implant it
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends Integer> c) {
        //I could not implant it
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        //I could not implant it
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        //I could not implant it
        return false;
    }

    @Override
    public void clear() {
        Integer[] arrayCleared = new Integer[0];
        array = arrayCleared;
        indexElements = 0;
    }

    @Override
    public Integer get(int index) {
        Integer valueReturned = 0;
        for(int currentPosition  = 0; currentPosition < array.length ; currentPosition++){
            if(currentPosition == index){
                valueReturned = array[currentPosition];
            }
        }
        return valueReturned;
    }

    @Override
    public Integer set(int index, Integer element) {
        //I could not implant it
        return null;
    }

    @Override
    public void add(int index, Integer element) {
        //I could not implant it
    }

    @Override
    public Integer remove(int index) {
        Integer[] provisionalArray = new Integer[array.length-1];
        int indexProvisionalArray = 0;
        for(int currentPosition = 0; currentPosition < array.length ; currentPosition++){
            if(currentPosition != index){
                provisionalArray[indexProvisionalArray] = array[currentPosition];
                indexProvisionalArray++;
            }
        }
        array = provisionalArray;
        indexElements--;
        return 0;
    }

    @Override
    public int indexOf(Object o) {
        //I could not implant it
        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        //I could not implant it
        return 0;
    }

    @Override
    public ListIterator<Integer> listIterator() {
        //I could not implant it
        return null;
    }

    @Override
    public ListIterator<Integer> listIterator(int index) {
        //I could not implant it
        return null;
    }

    @Override
    public List<Integer> subList(int fromIndex, int toIndex) {
        //I could not implant it
        return null;
    }
    public Integer[] getArray() {
        //I could not implant it
        return array;
    }
}
