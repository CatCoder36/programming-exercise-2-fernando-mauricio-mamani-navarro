package Operators;
import Stack.Stack;
import Stack.StackTools;
public class MultiplicationMath extends Operation {
    public MultiplicationMath() {
        super('*');
    }

    @Override
    public Stack<Character> executeOperation(int firstNumber, int secondNumber){
       return multiplyIntegers(firstNumber,secondNumber);
    }

    public Stack<Character> multiplyIntegers(int numberOne, int numberTwo){
        Stack<Character> resultStack;
        int result = numberOne * numberTwo;
        resultStack = StackTools.copyIntegerToStack(result);
        return resultStack;
    }
}
