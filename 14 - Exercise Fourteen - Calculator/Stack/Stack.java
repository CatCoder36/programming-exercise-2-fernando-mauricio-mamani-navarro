package Stack;

/**
 * This class is a Stack.Stack structure based on a linked list.
 *
 * @param <T> receives the data type the class will work with.
 * @author Fernando Mauricio Mamani Navarro
 */
public class Stack<T extends Comparable<T> > {
    private Node5<T> head;
    private int size;

    /**
     * This is the constructor method.
     * Initializes the values in null and zero.
     */
    public Stack(){
        head = null;
        size = 0;
    }

    /**
     * This method insert a value in the linked list head.
     *This method creates a new node with the value received by parameter,
     *  if the linked list is empty (head = null) inserts in the head of
     *  the linked list the new node, if it already had an element in the
     *  head the next data of the new node becomes the current head and
     *  the head takes the value of the new node.
     *
     * @param data, T type, receives the new valor to insert.
     */
    public void push(T data){
        Node5<T> nodeInserted = new Node5<>(data);
        if (head != null) {
            nodeInserted.setNext(head);
        }
        head = nodeInserted;
        size++;
    }

    /**
     * This method remove the las elements inserted in the linked list.
     * This method deletes the current head and returns the value of the
     * deleted node.
     *
     * @return T type, return the value removed.
     */
    public T pop(){
        T valueRemoved = head.getData();
        head = head.getNext();
        size--;
        return valueRemoved;
    }

    /**
     * This method is the toString.
     *
     * @return String type, returns information of the class and his values.
     */
    @Override
    public String toString() {
        return "Stack{}";
    }

    /**
     * This method generate a String with the values of the linked list.
     * This method helps us to make clearer tests.
     * @return String type, returns a String with the values of the linked list.
     */
    public String getElements(){
        StringBuilder elements = new StringBuilder();
        for (Node5<T> currentElement = head; currentElement != null; currentElement = currentElement.getNext()){
            elements.append(currentElement.getData()).append(", ");
        }
        return elements.toString();
    }
    public boolean isEmpty(){
        return head == null ;
    }
    public int getSize(){
        return size;
    }
}
