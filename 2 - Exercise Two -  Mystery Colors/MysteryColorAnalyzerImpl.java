import java.util.ArrayList;
import java.util.List;

/**
 * This class has the function of parsing color lists.
 * This class implements the interface MysteryColorAnalizer
 * The name of the class was defined according to the requirements of the task in CoderWars.
 */
public class MysteryColorAnalyzerImpl implements MysteryColorAnalyzer {

    @Override
    public int numberOfDistinctColors(List<Color> mysteryColors) {
        int quantityColors;
        ArrayList<Color> colorsFound = new ArrayList<>();
        for(Color currentColor: mysteryColors){
            if(!colorsFound.contains(currentColor)){
                System.out.println("entre");
                colorsFound.add(currentColor);
            }
        }
        quantityColors = colorsFound.size();
        return quantityColors;
    }

    @Override
    public int colorOccurrence(List<Color> mysteryColors, Color color) {
        int quantityColor = 0;
        for (Color currentColor : mysteryColors){
            if(currentColor.equals(color)){
                quantityColor++;
            }
        }
        return quantityColor;
    }
}
