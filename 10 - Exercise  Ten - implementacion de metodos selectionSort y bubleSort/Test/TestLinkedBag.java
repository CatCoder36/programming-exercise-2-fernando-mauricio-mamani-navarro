import org.junit.Assert;
import org.junit.Test;

public class TestLinkedBag {
    @Test
    public void bubbleSortedTest (){

        LinkedBag<Integer> linkedBag = new LinkedBag<>();
        linkedBag.add(1);
        linkedBag.add(2);
        linkedBag.add(3);
        linkedBag.add(4);
        linkedBag.add(5);
        linkedBag.add(6);
        linkedBag.add(7);
        linkedBag.add(8);
        linkedBag.add(9);
        linkedBag.add(10);
        linkedBag.add(11);
        linkedBag.add(12);
        linkedBag.add(13);
        linkedBag.add(14);

        linkedBag.bubbleSort();

        String expectedResult = "1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, ";
        Assert.assertEquals(expectedResult, linkedBag.getElements());

        linkedBag.add(0);
        linkedBag.add(16);
        linkedBag.add(54);
        linkedBag.add(-45);
        linkedBag.bubbleSort();
        expectedResult = "-45, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 16, 54, ";
        Assert.assertEquals(expectedResult, linkedBag.getElements());

        LinkedBag<Double> linkedBag2 = new LinkedBag<>();
        linkedBag2.add(4.77);
        linkedBag2.add(5.72);
        linkedBag2.add(1.98);
        linkedBag2.add(0.17);
        linkedBag2.add(69.75);

        linkedBag2.bubbleSort();
        expectedResult = "0.17, 1.98, 4.77, 5.72, 69.75, ";
        Assert.assertEquals(expectedResult, linkedBag2.getElements());

        LinkedBag<Double> linkedBag3 = new LinkedBag<>();
        linkedBag3.add(4.0);
        linkedBag3.add(4.0);
        linkedBag3.add(4.0);
        linkedBag3.add(4.0);
        linkedBag3.add(4.0);

        linkedBag3.bubbleSort();
        expectedResult = "4.0, 4.0, 4.0, 4.0, 4.0, ";
        Assert.assertEquals(expectedResult, linkedBag3.getElements());

    }

    @Test
    public void testSelectionSort(){
        LinkedBag<Integer> linkedBag = new LinkedBag<>();

        linkedBag.add(7);
        linkedBag.add(3);
        linkedBag.add(1);
        linkedBag.add(4);
        linkedBag.add(6);

        linkedBag.selectionSort();
        String expectedResult = "1, 3, 4, 6, 7, ";
        Assert.assertEquals(expectedResult, linkedBag.getElements());

        LinkedBag<Integer> linkedBag2 = new LinkedBag<>();
        linkedBag2.add(1);
        linkedBag2.add(2);
        linkedBag2.add(3);
        linkedBag2.add(4);
        linkedBag2.add(5);
        linkedBag2.add(6);
        linkedBag2.add(7);
        linkedBag2.add(8);
        linkedBag2.add(9);
        linkedBag2.add(10);
        linkedBag2.add(11);
        linkedBag2.add(12);
        linkedBag2.add(13);
        linkedBag2.add(14);

        linkedBag2.selectionSort();

        expectedResult = "1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, ";
        Assert.assertEquals(expectedResult, linkedBag2.getElements());

        LinkedBag<Double> linkedBag3 = new LinkedBag<>();
        linkedBag3.add(1.98);
        linkedBag3.add(2.58);
        linkedBag3.add(2.58);
        linkedBag3.add(9.28);
        linkedBag3.add(3.99);

        linkedBag3.selectionSort();
        expectedResult = "1.98, 2.58, 2.58, 3.99, 9.28, ";
        Assert.assertEquals(expectedResult, linkedBag3.getElements());

        LinkedBag<Double> linkedBag4 = new LinkedBag<>();
        linkedBag4.add(9.98);
        linkedBag4.add(9.98);
        linkedBag4.add(9.98);
        linkedBag4.add(9.98);
        linkedBag4.add(9.98);

        linkedBag4.selectionSort();
        expectedResult = "9.98, 9.98, 9.98, 9.98, 9.98, ";
        Assert.assertEquals(expectedResult, linkedBag4.getElements());

    }
}
