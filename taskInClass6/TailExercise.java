import Stack.Node5;

public class TailExercise<T extends Comparable<T> >  {
    Node10<T> tail;
    Node10<T> head;

    public TailExercise(){
        tail = null;
        head = null;
    }

    public void insertElement(T data){
        if(!searchElement(data)){
            if(tail == null){
                tail = new Node10(data, tail);
                head = tail;
            }
            else {
                tail = new Node10(data, tail);
            }
        }
    }

    private boolean searchElement(T data){
        boolean elementFound = false;
        for(Node10 currentElement = tail; currentElement != null; currentElement = currentElement.getNext()){
            if(currentElement.getData() == data){
                elementFound = true;
            }
        }
        return elementFound;
    }

    public T deque(){
        T valueRemoved = head.getData();
        boolean removed = false;
        for(Node10 currentNode = tail; tail.getNext() != null ; currentNode = currentNode.getNext() ){
            if(currentNode.getNext() == head){
                currentNode.setNext(null);
                head = currentNode;
                removed = true;
                break;
            }
        }
        if(tail.getNext() == null && !removed){
            tail = null;
            head = null;
        }

        return valueRemoved;
    }

    @Override
    public String toString() {
        return tail.toString();
    }

    public Object getTail(){
        return tail.getData();
    }

    public T getHead(){
        return head.getData();
    }

    public String getElements(){
        StringBuilder elements = new StringBuilder();
        for (Node10<T> currentElement = tail; currentElement != null; currentElement = currentElement.getNext()){
            elements.append(currentElement.getData()).append(", ");
        }
        return elements.toString();
    }
}

