class Canvas {
    private String[][] canva;
    private int width;
    private int height;
    public Canvas(int width, int height) {
        this.width = width;
        this.height = height;
        this.canva = new String[width][height];
        fillCanvas();
    }

    public Canvas draw(int y1, int x1, int y2, int x2) {
        if(x1 == x2){
            drawVerticalLine(x1, y1 , y2);
        }

        else if (y1 == y2){
            drawHorizontalLine(y1, x1, x2);
        }
        else{
            drawRectangle(x1,x2,y1,y2);
        }

        return this;
    }

    private void drawVerticalLine(int x, int y1 , int y2 ){
        for(int numberLine = y1 ; numberLine <= y2 ; numberLine++){
            canva[x][y1] = "x";
            y1++;
        }
    }

    private void drawHorizontalLine(int y, int x1 , int x2 ){
        for(int numberRow = x1 ; numberRow <= x2 ; numberRow++){
            canva[x1][y] = "x";
            x1++;
        }
    }

    private void drawRectangle (int x1 , int x2 , int y1 , int y2){
        drawVerticalLine(x1,y1,y2);
        drawVerticalLine(x2,y1,y2);
        drawHorizontalLine(y1,x1,x2);
        drawHorizontalLine(y2,x1,x2);
    }

    public String drawCanvas() {
        String canvaWithBorder = "";
        canvaWithBorder += drawTopLine()+"\n";
        canvaWithBorder += drawElementCanvas();
        canvaWithBorder += drawTopLine();
        return canvaWithBorder;
    }

    private void fillCanvas(){
        for (int numberLine = 0; numberLine < height ; numberLine++){
            for(int numberRow = 0 ; numberRow < width ; numberRow++){
                canva[numberRow][numberLine] = " ";
            }
        }
    }

    private String drawTopLine(){
        String elementBorder = "-";
        String lateralBorder = "";
        for(int numberRow = 0 ; numberRow < width+2 ; numberRow++ ){
            lateralBorder += elementBorder;
        }
        return lateralBorder;
    }

    private String drawElementCanvas(){
        String canvaWithElements = "";
        String verticalBorder ="|";
        for (int numberLine = 0; numberLine < height ; numberLine++){
            canvaWithElements += verticalBorder;
            for(int numberRow = 0 ; numberRow < width ; numberRow++){
                canvaWithElements += canva[numberRow][numberLine];
            }
            canvaWithElements += verticalBorder + "\n";
        }
        return canvaWithElements;
    }
}
