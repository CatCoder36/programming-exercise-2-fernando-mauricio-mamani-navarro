package Operators;

import Exceptions.NumberMajorToTheLimitException;
import Stack.*;
public class FactorialMath extends Operation {
    public FactorialMath() {
        super('!');
    }
    @Override
    public Stack<Character> executeFirstOperationFound(Stack<Character> mathExpression){
        Stack<Character> resultOperation;
        Stack<Character> resultStack = new Stack<Character>();
        Character currentCharacter;
        int previousValue = 0;
        boolean operationRealized = false;
        while (!mathExpression.isEmpty()){
            currentCharacter = mathExpression.pop();
            if(currentCharacter == getSymbol() && !operationRealized) {
                resultStack = StackTools.deleteElementsToTheStack(resultStack, (int)Math.log10(previousValue)+1);
                resultOperation = getIntegerFactorial(previousValue);
                resultStack = StackTools.mergeStacks(resultStack, resultOperation);
                operationRealized = true;
            } else {
                previousValue = mergeIntegerWithCharacters(previousValue, currentCharacter);
                resultStack.push(currentCharacter);
            }
        }
        return StackTools.invertStack(resultStack);
    }


    public Stack<Character> getIntegerFactorial(int number){
        Stack<Character> resultStack;
        int numberAnalyzed = number;
        int result = number;
        int laps = number;
        for(int index = 1; index < laps && calcException == null; index++){
            number--;
            result *= number;
            if(result < 0){
                calcException = new NumberMajorToTheLimitException(String.valueOf(numberAnalyzed)+"!");
            }
        }
        resultStack = StackTools.copyIntegerToStack(result);
        return resultStack;
    }


}
