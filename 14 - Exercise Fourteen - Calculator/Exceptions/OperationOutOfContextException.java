package Exceptions;

/**
  * This class is an Exception.
 * This exception is generated when an operator is misplaced or does not have a clear context of the operands.
 */
public class OperationOutOfContextException extends CalcException{
    /**
     * This is the constructor method
     *
     * @param elementInvalid, String type, receives the element invalid
     */
    public OperationOutOfContextException(String elementInvalid){
        super(elementInvalid);
    }

    /**
     * This method returns the character that generated the exception.
     *
     * @return String type, returns the character that generated the exception.
     */
    @Override
    public String getExceptionMessage(){
        return "Exception: This operation does not have a given context. : "+"<< "+getCharacterInvalid()+" >>";
    }
}
