import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class TestExercise5 {
    @Test
    public void basicTests() {
        ArraySumAnalyzer solution = new ArraySumAnalyzer();
        assertEquals(6, solution.addingNumberOfFollowedArray(new int[] {2, 1, 2, 2, 6, 5, 0, 2, 0, 5, 5, 7, 7, 4, 3, 3, 9}));
        assertEquals(5, solution.addingNumberOfFollowedArray(new int[] {2, 1, 2, 2, 6, 5, 0, 2, 0, 3, 3, 3, 9, 2}));
        assertEquals(1, solution.addingNumberOfFollowedArray(new int[] {2}));
        assertEquals(2, solution.addingNumberOfFollowedArray(new int[] {1,2}));
        assertEquals(1, solution.addingNumberOfFollowedArray(new int[] {1,1,2,2}));
    }
}
