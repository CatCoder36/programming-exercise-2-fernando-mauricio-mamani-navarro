public class CircularLinkedList<T extends Comparable<T>> implements List<T> {

    private Nodo<T> root;
    private int size;
    /**
     * This method helps us in the tests
     */
    public boolean remove(T data){
        return false;
    }
    /**
     * This method helps us in the tests
     */
    public boolean add(T data) {
        Nodo<T> node = new Nodo<>(data);
        if (root == null) {
            root = node;
        } else {
            Nodo<T> last = root;
            while(last.getNext() != root){last = last.getNext();}
            last.setNext(node);
        }
        node.setNext(root);
        size++;
        return true;
    }

    /**
     * This method consult the attribute size.
     *
     * @return int type, returns the length list.
     */
    public int size(){
        return size;
    }

    /**
     * This method verified if the list is Empty.
     * This method verified if root is null, if root is null the list is empty.
     *
     * @return boolean type, returns if the lists is empty.
     */
    public boolean isEmpty(){
        return (root == null);
    }

    /**
     * This method return a data in a specific position.
     * This method finds in the list a position and return the valor of the position.
     *
     * @param index, int type,  receives the position searched.
     * @return T type, returns the valor of the position.
     */
    public T get(int index){
        T valueReturned;
        Nodo<T> currentNode = root;
        for(int currentPosition = 0 ; currentPosition < index; currentPosition++){
            currentNode = currentNode.getNext();
        }
        valueReturned = currentNode.getData();
        return valueReturned;
    }

    /**
     * This method separates all the data of the list nodes into an array.
     * This method goes through the whole chain of Nodes and extracts their
     * values to arrange them in an array.
     *
     * @return Object[] type, returns an array with the values of the nodes.
     */
    private Object[] separateIntoArrays(){
        Object[] separateNodes = new Object[size];
        Nodo<T> currentNode = root;
        for(int index = 0; index < size; index++){
            separateNodes[index] = currentNode.getData();
            currentNode = currentNode.getNext();
        }
        return  separateNodes;
    }

    /**
     * This method sorts the values of the node elements using the merge sort pattern.
     * This method calls for the main actions to perform the debugging.
     *
     * @see #convertToLineLinkedList()
     * @see #sort(Object[], int, int)
     * @see #generateNewCircularLinkedList(Object[])
     */
    @Override
    public void mergeSort() {
        convertToLineLinkedList();
        Object[] datas = separateIntoArrays();
        int lengthList = datas.length;
        sort(datas,0,lengthList-1);
        generateNewCircularLinkedList(datas);
    }

    /**
     * This method converts the circular list to a linear list.
     * This method makes the current linked list no longer circular
     * in order to handle the data more easily, it makes the last element
     * of the node chain no longer point to the first node but to null.
     */
    private void convertToLineLinkedList(){
        Nodo<T> currentNode = root;
        for(int index = 0; index < size ; index++, currentNode = currentNode.getNext()){
            if ((index+1) == size){
                currentNode.setNext(null);
            }
        }
    }

    /**
     * This method creates a new circular list based on an array.
     * This method regenerates the circular linked list using an array that already
     * contains the ordered values of the nodes.
     *
     * @param datas Object[] type, receives an array with the sorted data's.
     */

    private void generateNewCircularLinkedList(Object datas[]){
        root = null;
        int numberElements = size;
        size = 0;
        T currentData;
        for (int currentPosition = 0; currentPosition < numberElements; currentPosition++){
            currentData = (T)datas[currentPosition];
            add(currentData);
        }
    }

    /**
     * This method breaks the list into small parts for later sorting.
     * This method through recursion splits the array many times until
     * the array is of size 1.
     *
     * @param datasList, Object[] type, receives the array with the elements to be sorted.
     * @param leftPosition int type, receives the left position in the split list.
     * @param rightPosition int type, receives the right position in the split list.
     */
    private void sort(Object datasList[], int leftPosition, int rightPosition){
        if(leftPosition < rightPosition){
            int middlePosition = (leftPosition + rightPosition) / 2;
            sort(datasList, leftPosition, middlePosition);
            sort(datasList, middlePosition+1, rightPosition);
            mergeDatas(datasList, leftPosition, middlePosition, rightPosition);
        }
    }

    /**
     * This method starts sorting the data in the list.
     * This method uses axillary lists to arrange and order the list of elements passed by parameter.
     *
     * @param datasList, Object[] type, receives the array with the elements to be sorted.
     * @param leftPosition int type, receives the left position in the split list.
     * @param rightPosition int type, receives the right position in the split list.
     * @param middlePosition, int type, receives the middle position in the split list,
     */
    private void mergeDatas(Object datasList[], int leftPosition, int middlePosition, int rightPosition) {
        int firstSublistLength = middlePosition - leftPosition + 1;
        int secondSublistLength = rightPosition - middlePosition;
        //AuxiliaryArrays
        Object leftArray[] = new Object[firstSublistLength];
        Object rightArray[] = new Object[secondSublistLength];

        for (int currentPosition = 0 ; currentPosition < firstSublistLength; currentPosition++) {
            leftArray[currentPosition] = datasList[leftPosition+currentPosition];
        }
        for (int currentPosition = 0; currentPosition < secondSublistLength; currentPosition ++) {
            rightArray[currentPosition] = datasList[middlePosition + currentPosition + 1];
        }
        int firstListIndex = 0;
        int secondListIndex = 0;

        int principalListIndex = leftPosition;
        //Sorting
        while (firstListIndex < firstSublistLength && secondListIndex < secondSublistLength) {
            T firstElement = (T) leftArray[firstListIndex];
            T secondElement = (T) rightArray[secondListIndex];
            if (secondElement.compareTo(firstElement)>=0) {
                datasList[principalListIndex] = leftArray[firstListIndex];
                firstListIndex++;
            } else {
                datasList[principalListIndex] = rightArray[secondListIndex];
                secondListIndex++;
            }
            principalListIndex++;
        }
        while (firstListIndex < firstSublistLength) {
            datasList[principalListIndex] = leftArray[firstListIndex];
            firstListIndex++;
            principalListIndex++;
        }
        while (secondListIndex < secondSublistLength) {
            datasList[principalListIndex] = rightArray[secondListIndex];
            secondListIndex++;
            principalListIndex++;
        }
    }

    /**
     * This is an auxiliary method that helps us to obtain a string with the values of the nodes.
     * This method helps us in the tests
     *
     * @return String type, returns a string with the nodes values.
     */
    public String getElements(){
        StringBuilder elements = new StringBuilder();
        Nodo<T> currentElement = root;
        int index = 0;
        for (; index < size; index++){
            elements.append(currentElement.getData()).append(", ");
            currentElement = currentElement.getNext();
        }
        return elements.toString();
    }

    /**
     * This method rotates the elements of the list the positions that are passed by parameter.
     * <p>
     *     This method rotates the elements of the list the spaces that are passed by paremeter,
     *     if a positive number is received by parameter it will rotate to the right the positions,
     *     but if a negative number is received it will rotate to the left.
     * </p>
     *
     * @param places, int type, receives the number of positions to be traversed.
     */
    public void rotate(int places){
        Nodo<T> currentNode;
        boolean reverseRotate = places < 0 ? true : false;
        places = Math.abs(places);
        for(int numberRotates = 0; numberRotates < places; numberRotates++){
            currentNode = root;
            for (int currentLap = 0; currentLap < size; currentLap++, currentNode = currentNode.getNext()){
                if(currentNode.getNext().equals(root) && !(reverseRotate)){
                    root = currentNode;
                }
                else if(currentLap == 0 && reverseRotate){
                   root = currentNode.getNext();
                }
            }
        }
    }

    /**
     * This method helps us in the tests
     */
    @Override
    public void bubbleSort() {}
    /**
     * This method helps us in the tests
     */
    @Override
    public void selectionSort() {

    }

}
