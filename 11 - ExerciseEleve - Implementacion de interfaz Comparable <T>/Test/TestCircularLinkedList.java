import org.junit.Assert;
import org.junit.Test;

public class TestCircularLinkedList {

    @Test
    public void testIsEmpty(){
        CircularLinkedList<Integer> circularLinkedList = new CircularLinkedList<>();
        Assert.assertEquals(true, circularLinkedList.isEmpty());

        circularLinkedList.add(4);
        Assert.assertEquals(false , circularLinkedList.isEmpty());

        CircularLinkedList<String> circularLinkedList1 = new CircularLinkedList<>();
        Assert.assertEquals(true, circularLinkedList1.isEmpty());

        circularLinkedList1.add("Text1");
        Assert.assertEquals(false , circularLinkedList1.isEmpty());
    }

    @Test
    public void testGet(){
        CircularLinkedList<Integer> circularLinkedList = new CircularLinkedList<>();
        circularLinkedList.add(1);
        circularLinkedList.add(4);
        circularLinkedList.add(9);
        circularLinkedList.add(8);
        circularLinkedList.add(2);
        circularLinkedList.add(5);

        Assert.assertEquals(1, (int)circularLinkedList.get(0));
        Assert.assertEquals(4, (int)circularLinkedList.get(1));
        Assert.assertEquals(9, (int)circularLinkedList.get(2));
        Assert.assertEquals(8, (int)circularLinkedList.get(3));
        Assert.assertEquals(2, (int)circularLinkedList.get(4));
        Assert.assertEquals(5, (int)circularLinkedList.get(5));
        Assert.assertEquals(1, (int)circularLinkedList.get(6));
        Assert.assertEquals(4, (int)circularLinkedList.get(7));

        CircularLinkedList<String> circularLinkedList1 = new CircularLinkedList<>();
        circularLinkedList1.add("Text1");
        circularLinkedList1.add("Text2");
        circularLinkedList1.add("Text3");
        circularLinkedList1.add("Text4");
        circularLinkedList1.add("Text5");

        Assert.assertEquals("Text1", circularLinkedList1.get(0));
        Assert.assertEquals("Text2", circularLinkedList1.get(1));
        Assert.assertEquals("Text3", circularLinkedList1.get(2));
        Assert.assertEquals("Text4", circularLinkedList1.get(3));
        Assert.assertEquals("Text5", circularLinkedList1.get(4));
        Assert.assertEquals("Text1", circularLinkedList1.get(5));
        Assert.assertEquals("Text2", circularLinkedList1.get(6));

    }

    @Test
    public void testMergeSorted(){
        CircularLinkedList<Integer> circularLinkedList = new CircularLinkedList();
        circularLinkedList.add(1);
        circularLinkedList.add(4);
        circularLinkedList.add(9);
        circularLinkedList.add(8);
        circularLinkedList.add(2);
        circularLinkedList.add(5);
        circularLinkedList.add(14);
        circularLinkedList.add(17);

        circularLinkedList.mergeSort();
        String expectedResult = "1, 2, 4, 5, 8, 9, 14, 17, ";
        Assert.assertEquals(expectedResult, circularLinkedList.getElements());

        circularLinkedList.add(3);
        circularLinkedList.add(7);
        circularLinkedList.mergeSort();
        expectedResult = "1, 2, 3, 4, 5, 7, 8, 9, 14, 17, ";
        Assert.assertEquals(expectedResult, circularLinkedList.getElements());

        CircularLinkedList<Integer> circularLinkedList4 = new CircularLinkedList<>();
        circularLinkedList4.add(1);
        circularLinkedList4.mergeSort();
        expectedResult = "1, ";
        Assert.assertEquals(expectedResult, circularLinkedList4.getElements());

        CircularLinkedList<Integer> circularLinkedList5 = new CircularLinkedList<>();
        circularLinkedList5.mergeSort();
        expectedResult = "";
        Assert.assertEquals(expectedResult,circularLinkedList5.getElements());

        CircularLinkedList<String> circularLinkedList1 = new CircularLinkedList<>();
        circularLinkedList1.add("Text1");
        circularLinkedList1.add("Text2");
        circularLinkedList1.add("Text9");
        circularLinkedList1.add("Text4");
        circularLinkedList1.mergeSort();
        expectedResult = "Text1, Text2, Text4, Text9, ";
        Assert.assertEquals(expectedResult, circularLinkedList1.getElements());

        CircularLinkedList<String> circularLinkedList2 = new CircularLinkedList<>();
        circularLinkedList2.add("h");
        circularLinkedList2.add("hello");
        circularLinkedList2.add("hel");
        circularLinkedList2.add("hell");
        circularLinkedList2.add("hello!");
        circularLinkedList2.mergeSort();
        expectedResult = "h, hel, hell, hello, hello!, ";
        Assert.assertEquals(expectedResult, circularLinkedList2.getElements());

        CircularLinkedList<String> circularLinkedList7 = new CircularLinkedList<>();
        circularLinkedList7.add("h");
        circularLinkedList7.mergeSort();
        expectedResult = "h, ";
        Assert.assertEquals(expectedResult, circularLinkedList7.getElements());

        CircularLinkedList<String> circularLinkedList8 = new CircularLinkedList<>();
        circularLinkedList5.mergeSort();
        expectedResult = "";
        Assert.assertEquals(expectedResult,circularLinkedList8.getElements());
    }

    @Test
    public void testRotate(){
        CircularLinkedList<Integer> circularLinkedList = new CircularLinkedList();
        circularLinkedList.add(1);
        circularLinkedList.add(2);
        circularLinkedList.add(3);
        circularLinkedList.add(4);
        circularLinkedList.add(5);
        circularLinkedList.add(6);
        circularLinkedList.rotate(1);
        String expectedResult = "6, 1, 2, 3, 4, 5, ";
        Assert.assertEquals(expectedResult, circularLinkedList.getElements());

        circularLinkedList.rotate(3);
        expectedResult = "3, 4, 5, 6, 1, 2, ";
        Assert.assertEquals(expectedResult, circularLinkedList.getElements());
        Assert.assertEquals((Integer)4, circularLinkedList.get(1));

        circularLinkedList.rotate(8);
        expectedResult = "1, 2, 3, 4, 5, 6, ";
        Assert.assertEquals(expectedResult, circularLinkedList.getElements());

        circularLinkedList.rotate(-1);
        expectedResult = "2, 3, 4, 5, 6, 1, ";
        Assert.assertEquals(expectedResult, circularLinkedList.getElements());

        circularLinkedList.rotate(-4);
        expectedResult = "6, 1, 2, 3, 4, 5, ";
        Assert.assertEquals(expectedResult, circularLinkedList.getElements());

        circularLinkedList.rotate(-7);
        expectedResult = "1, 2, 3, 4, 5, 6, ";
        Assert.assertEquals(expectedResult, circularLinkedList.getElements());

    }
}
