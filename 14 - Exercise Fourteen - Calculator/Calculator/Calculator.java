package Calculator;

import Exceptions.CalcException;
import Operators.*;
import Stack.*;
import Operators.Operation;

/**
 * This class manage the calculator logic
 *  This is the main class, it manages the other classes to perform the
 *  operations and validations.
 *
 * @author Fernando Mauricio Mamani Navarro
 */
public class Calculator {

    private Stack<Character> currentMathematicalExpression;
    private Stack<Operation> operations;
    private OperationValidator operationValidator;

    private CalcException calcException;

    /**
     * This is constructor method.
     * This method initializes the class fields and call to fillOperation() method.
     *
     * @see #fillOperations()
     */
    public Calculator() {
        this.calcException = null;
        this.currentMathematicalExpression = new Stack<Character>();
        this.operations = new Stack<Operation>();
        this.operationValidator = new OperationValidator();
        fillOperations();
    }

    /**
     * This method receives the operation to be performed.
     * Calls to check the syntax of the operation and if there are no exceptions, performs first
     * the operations in parentheses and then the rest. If there is an exception, the operations
     * will not be performed and the exception will be thrown.
     *
     * @see #insertTokens(String) 
     * @see OperationValidator#verifyOperation(Stack)
     * @see #analyzingOperation(Stack)
     * @see #launchException() 
     * @param operation, String type, receives the operation to resolve;
     * @return int type, return the result operation.
     */
    public int calculate(String operation) {
        calcException = null;
        currentMathematicalExpression = insertTokens(operation);
        int resultOperation = 0;
        calcException = operationValidator.verifyOperation(currentMathematicalExpression);
        if (calcException == null) {
            resultOperation = analyzingOperation(currentMathematicalExpression);
        } else {
            launchException();
        }
        return resultOperation;
    }

    /**
     * This method print the Exception message found.
     * this method uses the exception stored by the class and calls the exception stored by this class
     * to print its message
     */
    private void launchException(){
        System.out.println(calcException.getExceptionMessage());
    }

    /**
     * This method fill the stack operation.
     * Because the stack clears its elements, this method reloads the values there.
     */
    private void fillOperations(){
        operations.push(new MultiplicationMath());
        operations.push(new SumMath());
        operations.push(new PowerMath());
        operations.push(new FactorialMath());
    }

    /**
     * This method change the String received to Stack.
     * In this method we receive the mathematical expression as a String and we put each
     * digit into a stack. At the end of the operation the stack is inverted to keep the
     * operation orderly
     * 
     * @see StackTools#invertStack(Stack) 
     * @param operation, String type, receives the mathematical Expression.
     * @return Stack<Character<> type, returns a stack with characters of the mathematical Expression.
     */
    private Stack<Character> insertTokens(String operation) {
        Stack<Character> tokens = new Stack<Character>();
        for (int i = 0; i < operation.length(); i++) {
            tokens.push(operation.charAt(i));
        }
        return StackTools.invertStack(tokens);
    }

    /**
     * This method resolve all mathematics operations found.
     * This method goes through the stack with the operation, in case it finds an operation it solves all
     * operations of that type until there are no more operations of that type.
     * In case one of the operations generates an exception, the exception will be thrown and the returned 
     * result will be 0.
     * 
     * @see Operation#executeFirstOperationFound(Stack)
     * @see #launchException() 
     * @see StackTools#convertStackCharacterToInt(Stack)
     * @param currentMathExpression Stack<Character> type, receives a Stack with a Mathematical expression.
     * @return int type, returns the operation result.
     */
    private int solveOperations(Stack<Character> currentMathExpression){
        int result = 0;
        Operation currentOperation;
        boolean exceptionFound = false;
        while (!operations.isEmpty() && !exceptionFound){
            currentOperation = operations.pop();
            while (searchOperator(currentMathExpression, currentOperation.getSymbol())){
                currentMathExpression = currentOperation.executeFirstOperationFound(currentMathExpression);
            }
            if(currentOperation.getCalcException() != null){
                calcException = currentOperation.getCalcException();
                launchException();
                exceptionFound = true;
            }
        }
        if(calcException == null) {
            result = StackTools.convertStackCharacterToInt(currentMathExpression);
        }
        fillOperations();
        return result;
    }

    /**
     * This method analyze if operation contains a parenthesis.
     * This method checks if there is parenthesis in the expression, in case there is, it sends to solve all the
     * operations with parenthesis first and then the operations without parenthesis.
     * 
     * @see #resolveOperationInParentheses(Stack)
     * @see #solveOperations(Stack)
     * @see #searchOperator(Stack, Character)
     * @param currentMathExpression Stack<Character> type, receives the mathematical expression
     * @return int type, returns the operation result
     */
    private int analyzingOperation(Stack<Character> currentMathExpression){
        int result = 0;
        while (searchOperator(currentMathExpression, '(')){
            currentMathExpression = resolveOperationInParentheses(currentMathExpression);
        }
        if(calcException == null){
            result = solveOperations(currentMathExpression);
        }
        return result;
    }

    /**
     * This method resolve the first operation in parentheses.
     * This method finds the innermost parenthesis of the operation, once found, it collects all the operation within
     * this parenthesis and solves it, the result occupies the place occupied by the parenthesis and the whole operation
     * is returned.
     *
     * @see #isTheFirstParenthesis(Character, Stack) 
     * @see #collectingOperationInParenthesis(Character, Stack) 
     * @see #solveOperations(Stack) 
     * @see StackTools#mergeStacks(Stack, Stack)
     * @param currentMathExpression Stack<Character> type, receives the mathematical expression
     * @return Stack<Character> type, returns the Stack with parentheses resolved
     */
    private Stack<Character> resolveOperationInParentheses(Stack<Character> currentMathExpression){
        Stack<Character> operationInParentheses;
        Stack<Character> resultStack = new Stack<Character>();
        Character currentCharacter;
        int resultOperation;
        while (!currentMathExpression.isEmpty()){
            currentCharacter = currentMathExpression.pop();
            if(isTheFirstParenthesis(currentCharacter,currentMathExpression)){
                currentCharacter = currentMathExpression.pop();
                operationInParentheses = collectingOperationInParenthesis(currentCharacter, currentMathExpression);
                resultOperation = solveOperations(operationInParentheses);
                operationInParentheses = StackTools.copyIntegerToStack(resultOperation);
                resultStack = StackTools.mergeStacks(resultStack,operationInParentheses);
            }
            else {
                resultStack.push(currentCharacter);
            }
        }
        return StackTools.invertStack(resultStack);
    }

    /**
     * This method collect a mathematical expression in the parentheses.
     * This method goes through the list generating a stack with the mathematical expression, the collection stops when
     * the closed parenthesis symbol is encountered.
     *
     * @param currentCharacter Character type, receives the current character evaluated.
     * @param currentMathExpression Stack<Character> type, receives the mathematical expression
     * @return Stack<Character> type, returns the Stack with the mathematical expression collected.
     */

    private Stack<Character> collectingOperationInParenthesis(Character currentCharacter, Stack<Character> currentMathExpression){
        Stack<Character> operationInParentheses = new Stack<Character>();
        while(currentCharacter != ')'){
            operationInParentheses.push(currentCharacter);
            currentCharacter = currentMathExpression.pop();
        }
        return StackTools.invertStack(operationInParentheses);
    }

    /**
     * This method verify if a parenthesis is the es first internal parenthesis.
     * This method checks if there are more open parenthesis signs in the operation.
     *
     * @param character Character type, receives the current character evaluated.
     * @param mathExpression tack<Character> type, receives the mathematical expression
     * @return boolean type, returns if the parenthesis inserted is the es first internal parenthesis
     */
    private boolean isTheFirstParenthesis(Character character, Stack<Character> mathExpression){
        return character == '(' && !searchOperator(mathExpression, '(');
    }

    /**
     * This method search a character in a Stack with a mathematical expression.
     * This method traverses a stack looking for a character and returns true if found.
     *
     * @param stackWithOperations Stack<Character> type, receives the mathematical expression
     * @param operatorSearched Character type, receives the current character searched.
     * @return boolean type,  returns true if character found
     */
    private boolean searchOperator(Stack<Character> stackWithOperations, Character operatorSearched){
        boolean operationFound = false;
        Character currentCharacter;
        Stack<Character> stackBackup = new Stack<Character>();
        while(!stackWithOperations.isEmpty()){
            currentCharacter = stackWithOperations.pop();
            if(currentCharacter == operatorSearched){
                operationFound = true;
            }
            stackBackup.push(currentCharacter);
        }
        stackWithOperations = StackTools.copyStackToStack(stackWithOperations, stackBackup);
        return operationFound;
    }

    /**
     * This method returns the CalcException value.
     *
     * @return CalcException type, returns the CalcException value.
     */
    public CalcException getCalcException(){
        return calcException;
    }
}