package Exceptions;

/**
 * This class handles the exceptions that occur in the Calculator class.
 *
 * @author Fernando Mauricio Mamani Navarro
 */
public class CalcException {
    private String exceptionMessage;
    private String characterInvalid;

    /**
     * This is the constructor method;
     */
    public CalcException(){
        this.characterInvalid = ".";
    }

    /**
     * This is the second constructor method;
     */
    public CalcException(String character){
        this.characterInvalid = character;
    }

    /**
     * This method returns the message of the exception.
     *
     * @return String type, returns the message of the exception.
     */
    public String getExceptionMessage(){
        return "Invalid operation : "+"<< "+characterInvalid+" >>";
    }

    /**
     * This method returns the character that generated the exception.
     *
     * @return String type, returns the character that generated the exception.
     */
    public String getCharacterInvalid(){
        return characterInvalid;
    }

}
