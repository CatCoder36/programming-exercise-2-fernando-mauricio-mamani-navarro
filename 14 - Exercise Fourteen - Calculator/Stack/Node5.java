package Stack;

public class Node5<T> {
    private T data;
    private Node5<T> next;

    public Node5<T> getNext() {
        return next;
    }
    public void setNext(Node5<T> next) {
        this.next = next;
    }
    public T getData() {
        return data;
    }

    public Node5(T data) {
        this.data = data;
    }
    public String toString() {
        return "{data=" + data + ", sig->" + next + "}";
    }

}
