class Node2 {

    int data;
    Node2 next = null;

    Node2(final int data) {
        this.data = data;
    }

    public Node2(int data, Node2 next) {
        this.data = data;
        this.next = next;
    }

    public static Node2 append(Node2 listA, Node2 listB) {
        Node2 nodeReturned = listA;
        Node2 auxiliaryNode = null;
        if (listA != null || listB != null) {
            if (nodeReturned == null) {
                nodeReturned = listB;
            } else {
                auxiliaryNode = nodeReturned;
                while (auxiliaryNode.next != null) {
                    auxiliaryNode = auxiliaryNode.next;
                }
                auxiliaryNode.next = listB;
            }
        } else {
            nodeReturned = null;
        }
        return nodeReturned;
    }
}