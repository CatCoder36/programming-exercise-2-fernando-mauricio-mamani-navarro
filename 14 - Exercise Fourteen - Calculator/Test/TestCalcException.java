import Calculator.Calculator;
import Exceptions.CalcException;
import org.junit.Assert;
import org.junit.Test;

public class TestCalcException {
    @Test
    public void testNumberMajorToTheLimitException(){
        Calculator calculator = new Calculator();
        calculator.calculate("2147483648+0");
        CalcException calcException = calculator.getCalcException();
        String expectedExceptionMessage = "Exception : The inserted number exceeds the allowed limit. : << 2147483648 >>";
        String currentMessageException = calcException.getExceptionMessage();

        Assert.assertEquals(expectedExceptionMessage, currentMessageException);

        calculator.calculate("5456454654654542+54654");
        calcException = calculator.getCalcException();
        currentMessageException = calcException.getExceptionMessage();
        expectedExceptionMessage = "Exception : The inserted number exceeds the allowed limit. : << 5456454654 >>";

        Assert.assertEquals(expectedExceptionMessage, currentMessageException);

        calculator.calculate("2147483647+1");
        calcException = calculator.getCalcException();
        currentMessageException = calcException.getExceptionMessage();
        expectedExceptionMessage = "Exception : The inserted number exceeds the allowed limit. : << 2147483647+1 >>";

        Assert.assertEquals(expectedExceptionMessage, currentMessageException);

        calculator.calculate("1000!");
        calcException = calculator.getCalcException();
        currentMessageException = calcException.getExceptionMessage();
        expectedExceptionMessage = "Exception : The inserted number exceeds the allowed limit. : << 1000! >>";

        Assert.assertEquals(expectedExceptionMessage, currentMessageException);

    }
    @Test
    public void testOperationOutOfContextException(){
        Calculator calculator = new Calculator();
        calculator.calculate("2++3");
        CalcException calcException = calculator.getCalcException();
        String expectedExceptionMessage = "Exception: This operation does not have a given context. : << + >>";
        String currentMessageException = calcException.getExceptionMessage();

        Assert.assertEquals(expectedExceptionMessage, currentMessageException);

        calculator.calculate("1000**");
        calcException = calculator.getCalcException();
        currentMessageException = calcException.getExceptionMessage();
        expectedExceptionMessage = "Exception: This operation does not have a given context. : << * >>";

        Assert.assertEquals(expectedExceptionMessage, currentMessageException);

        calculator.calculate("4+*7");
        calcException = calculator.getCalcException();
        currentMessageException = calcException.getExceptionMessage();
        expectedExceptionMessage = "Exception: This operation does not have a given context. : << + >>";

        Assert.assertEquals(expectedExceptionMessage, currentMessageException);

        calculator.calculate("14^^^5");
        calcException = calculator.getCalcException();
        currentMessageException = calcException.getExceptionMessage();
        expectedExceptionMessage = "Exception: This operation does not have a given context. : << ^ >>";

        Assert.assertEquals(expectedExceptionMessage, currentMessageException);
    }

    @Test
    public void testOperatorInvalidException(){
        Calculator calculator = new Calculator();
        calculator.calculate("7-5");
        CalcException calcException = calculator.getCalcException();
        String expectedExceptionMessage = "Exception: This operator is not valid to perform operations: << - >>";
        String currentMessageException = calcException.getExceptionMessage();

        Assert.assertEquals(expectedExceptionMessage, currentMessageException);

        calculator.calculate("(14/2)+8");
        calcException = calculator.getCalcException();
        currentMessageException = calcException.getExceptionMessage();
        expectedExceptionMessage = "Exception: This operator is not valid to perform operations: << / >>";

        Assert.assertEquals(expectedExceptionMessage, currentMessageException);

        calculator.calculate("4%5+48^2");
        calcException = calculator.getCalcException();
        currentMessageException = calcException.getExceptionMessage();
        expectedExceptionMessage = "Exception: This operator is not valid to perform operations: << % >>";

        Assert.assertEquals(expectedExceptionMessage, currentMessageException);

        calculator.calculate("(9+4)/4");
        calcException = calculator.getCalcException();
        currentMessageException = calcException.getExceptionMessage();
        expectedExceptionMessage = "Exception: This operator is not valid to perform operations: << / >>";

        Assert.assertEquals(expectedExceptionMessage, currentMessageException);
    }

    @Test
    public void testParenthesesIncompleteException(){
        Calculator calculator = new Calculator();
        calculator.calculate("((7*5)");
        CalcException calcException = calculator.getCalcException();
        String expectedExceptionMessage = "Exception: Incomplete parentheses, excess parentheses : << ( >>";
        String currentMessageException = calcException.getExceptionMessage();

        Assert.assertEquals(expectedExceptionMessage, currentMessageException);

        calculator.calculate("4+((9+4)/4)+4)");
        calcException = calculator.getCalcException();
        currentMessageException = calcException.getExceptionMessage();
        expectedExceptionMessage ="Exception: Incomplete parentheses, excess parentheses : << ) >>";

        Assert.assertEquals(expectedExceptionMessage, currentMessageException);

        calculator.calculate("8*7+((((4+8)");
        calcException = calculator.getCalcException();
        currentMessageException = calcException.getExceptionMessage();
        expectedExceptionMessage = "Exception: Incomplete parentheses, excess parentheses : << ( >>";

        Assert.assertEquals(expectedExceptionMessage, currentMessageException);
    }
}
