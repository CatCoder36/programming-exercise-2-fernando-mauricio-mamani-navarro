import org.junit.Assert;
import org.junit.Test;

public class TestStack {
    @Test
    public void testPush(){
        Stack<Integer> stack = new Stack<>();
        stack.push(5);
        stack.push(6);
        stack.push(7);
        stack.push(8);
        stack.push(9);
        stack.push(10);
        String expectedResult =  "10, 9, 8, 7, 6, 5, ";
        Assert.assertEquals(expectedResult, stack.getElements());

        stack.push(11);
        expectedResult = "11, 10, 9, 8, 7, 6, 5, ";
        Assert.assertEquals(expectedResult, stack.getElements());

        //Test with Strings

        Stack<String> stackString = new Stack<>();
        expectedResult = "";
        Assert.assertEquals(expectedResult, stackString.getElements());

        stackString.push("Text1");
        stackString.push("Text2");
        stackString.push("Text3");

        expectedResult = "Text3, Text2, Text1, ";
        Assert.assertEquals(expectedResult, stackString.getElements());
    }

    @Test
    public void testPop(){
        Stack<Integer> stack = new Stack<>();
        stack.push(5);
        stack.push(6);
        stack.push(7);
        stack.push(8);
        stack.push(9);
        stack.push(10);
        //deleting last element inserted(10)...
        stack.pop();

        String expectedResult = "9, 8, 7, 6, 5, ";
        Assert.assertEquals(expectedResult, stack.getElements());

        stack.pop();
        stack.pop();

        expectedResult = "7, 6, 5, ";
        Assert.assertEquals(expectedResult, stack.getElements());

        //test with strings;

        Stack<String> stackString= new Stack<>();
        stackString.push("Text1");
        stackString.push("Text2");
        stackString.push("Text3");
        stackString.push("Text4");
        stackString.push("Text5");

        stackString.pop();
        expectedResult = "Text4, Text3, Text2, Text1, ";
        Assert.assertEquals(expectedResult, stackString.getElements());

        stackString.pop();
        stackString.pop();
        stackString.pop();

        expectedResult = "Text1, ";
        Assert.assertEquals(expectedResult, stackString.getElements());

        stackString.pop();
        expectedResult = "";
        Assert.assertEquals(expectedResult, stackString.getElements());
    }

    @Test
    public void testPushAndPop(){
        Stack<Integer> stack = new Stack<>();
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        stack.push(5);

        String expectedResult = "5, 4, 3, 2, 1, ";
        Assert.assertEquals(expectedResult, stack.getElements());

        stack.pop();
        stack.pop();

        expectedResult = "3, 2, 1, ";
        Assert.assertEquals(expectedResult, stack.getElements());

        stack.push(10);
        stack.push(11);
        stack.push(12);
        expectedResult = "12, 11, 10, 3, 2, 1, ";
        Assert.assertEquals(expectedResult, stack.getElements());

        //Test with Strings;

        Stack<String> stringStack = new Stack<>();
        stringStack.push("Hello1");
        stringStack.push("Hello2");
        stringStack.push("Hello3");

        expectedResult = "Hello3, Hello2, Hello1, ";
        Assert.assertEquals(expectedResult, stringStack.getElements());

        stringStack.pop();
        stringStack.pop();

        expectedResult = "Hello1, ";
        Assert.assertEquals(expectedResult, stringStack.getElements());

        stringStack.push("Bay1");
        stringStack.push("Bay2");

        expectedResult = "Bay2, Bay1, Hello1, ";
        Assert.assertEquals(expectedResult, stringStack.getElements());
    }
}
