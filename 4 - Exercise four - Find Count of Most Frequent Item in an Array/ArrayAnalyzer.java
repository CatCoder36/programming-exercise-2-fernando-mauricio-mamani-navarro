import java.lang.reflect.Array;
import java.util.Arrays;

/**
 * This class analyzes arrays of integers.
 * The purpose of this class is to analyze arrays
 * of integers and return the results.
 */
public class ArrayAnalyzer {
    /**
     * This method analyzes the maximum number of times an element is repeated.
     * This method goes through the array passed by parameter and counts the element
     * that is repeated more times.
     *
     * @param arrayAnalyzed int[] type, received the array analyzed.
     * @return int type, return de maximum number of repetitions of an element
     */
    public int searchNumberFrequentlyElement(int[] arrayAnalyzed){

        Arrays.sort(arrayAnalyzed);
        int numberFrequently = 0;
        int maxNumberFined = 0;
        int numberActualEvaluated = (arrayAnalyzed.length == 0) ? 0 : arrayAnalyzed[0];
        for(int currentNumber : arrayAnalyzed){
            if(currentNumber == numberActualEvaluated){
                maxNumberFined++;
            }
            else{
                if(maxNumberFined > numberFrequently){
                    numberFrequently = maxNumberFined;
                }
                numberActualEvaluated = currentNumber;
                maxNumberFined = 1;
            }
        }
        return numberFrequently;
    }
}