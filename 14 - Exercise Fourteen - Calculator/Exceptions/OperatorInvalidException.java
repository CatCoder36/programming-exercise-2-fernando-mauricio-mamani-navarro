package Exceptions;

/**
 * This class is an Exception.
 * This exception is generated when an operator is not valid for an operation.
 */
public class OperatorInvalidException extends CalcException{
    /**
     * This is the constructor method
     *
     * @param invalidCharacter, String type, receives the element invalid
     */
    public OperatorInvalidException(String invalidCharacter) {
        super(invalidCharacter);
    }

    /**
     * This method returns the character that generated the exception.
     *
     * @return String type, returns the character that generated the exception.
     */
    @Override
    public String getExceptionMessage() {
        return "Exception: This operator is not valid to perform operations: "+"<< "+super.getCharacterInvalid()+" >>";
    }
}
