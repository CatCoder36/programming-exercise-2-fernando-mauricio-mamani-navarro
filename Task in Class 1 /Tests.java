import org.junit.Assert;
import org.junit.Test;

public class Tests {
    static Player player = new Player(10,10);
    @Test
    public void testMovementPlayer(){

        int valueEvaluated = player.getActualPositionY();
        player.movePlayer(Movements.UP);
        int valueExpected = player.getActualPositionY();
        Assert.assertEquals(valueEvaluated-1,valueExpected);

        valueEvaluated = player.getActualPositionY();
        player.movePlayer(Movements.DOWN);
        valueExpected = player.getActualPositionY();
        Assert.assertEquals(valueEvaluated+1,valueExpected);

        valueEvaluated = player.getActualPositionX();
        player.movePlayer(Movements.LEFT);
        valueExpected = player.getActualPositionX();
        Assert.assertEquals(valueEvaluated-1,valueExpected);

        valueEvaluated = player.getActualPositionX();
        player.movePlayer(Movements.RIGHT);
        valueExpected = player.getActualPositionX();
        Assert.assertEquals(valueEvaluated+1,valueExpected);
    }
    @Test
    public void testBoard(){
        Board board = new Board();
        String boardTest[][] = board.getBoard();
        boardTest[5][5] = "HI";
        boolean valueExpected = board.checkAvailableCell(5,5);
        Assert.assertEquals(false,valueExpected);

        player.newPosition(9,9);
        valueExpected = board.verifyWinner(player);
        Assert.assertEquals(true,valueExpected);
    }
}
