/**
 * This class manage the Player properties.
 * @author Fernando Mauricio Mamani Navarro
 */
public class Player {
    private int actualPositionX;
    private int actualPositionY;
    private String icon;

    /**
     * This is the constructor method.
     *
     * @param maxPositionInX, int type, receives the max number in X coordinate.
     * @param maxPositionInY, int type, receives the max number in Y coordinate.
     * @see #generateIcon()
     */
    public Player(int maxPositionInX , int maxPositionInY ){
        actualPositionX = (int)(Math.random()*maxPositionInX+0);
        actualPositionY = (int)(Math.random()*maxPositionInY+0);
        this.icon = generateIcon();
    }

    /**
     * this method modify the player position.
     *
     * @param movement Movement type, receives the movement value.
     */
    public void movePlayer(Movements movement){
        switch (movement){
            case UP:
                actualPositionY --;
                break;
            case DOWN:
                actualPositionY ++;
                break;
            case LEFT:
                actualPositionX --;
                break;
            case RIGHT:
                actualPositionX ++;
                break;
        }
    }

    /**
     * This method generate the player icon.
     *
     * @return String type, returns a icon.
     */
    private String generateIcon(){
        String[] icons = new String[]{"0","Z","M","L","P","G","H","J"};
        int positionRandom = (int) (Math.random()*(icons.length-1)+0);
        String icon = icons[positionRandom];
        return icon;
    }
    /**
     * This method returns the icon
     *
     * @return String value ,returns the icon
     */
    public String getIcon() {
        return icon;
    }

    /**
     * This method returns the X position.
     *
     * @return int type, returns the X position.
     */
    public int getActualPositionX() {
        return actualPositionX;
    }
    /**
     * This method returns the Y position.
     *
     * @return int type, returns the Y position.
     */
    public int getActualPositionY() {
        return actualPositionY;
    }

    /**
     * this method is used to test
     * @param x
     */
    public void newPosition(int x, int y){
        actualPositionX = x;
        actualPositionY = y;
    }
}
