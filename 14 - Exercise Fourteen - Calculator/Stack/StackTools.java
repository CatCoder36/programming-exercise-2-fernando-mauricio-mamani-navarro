package Stack;

public class StackTools {
    public static Stack<Character> deleteElementsToTheStack(Stack<Character> stack, int quantityToEliminate){
        for(int index = 0; index < quantityToEliminate; index++){
            stack.pop();
        }
        return stack;
    }

    public static Stack<Character> mergeStacks(Stack<Character> stackOne, Stack<Character> stackTwo){
        while (!stackTwo.isEmpty()){
            stackOne.push(stackTwo.pop());
        }
        return stackOne;
    }

    public static Stack<Character> invertStack(Stack<Character> stack){
        Stack<Character> stackInverted = new Stack<Character>();
        int sizeStack = stack.getSize();
        for(int index = 0; index < sizeStack; index++){
            stackInverted.push(stack.pop());
        }
        return stackInverted;
    }

    public static Stack<Character> copyIntegerToStack(int numberAnalyzed){
        Stack<Character> resultStack = new Stack<Character>();
        int numberInserted;
        while (numberAnalyzed > 0){
            numberInserted = numberAnalyzed%10;
            resultStack.push((char) (numberInserted+'0'));
            numberAnalyzed = numberAnalyzed / 10;
        }
        return resultStack;
    }

    public static Stack<Character> copyStackToStack(Stack<Character> stackDestiny, Stack<Character> stackToClone){
        while(!stackToClone.isEmpty()){
            stackDestiny.push(stackToClone.pop());
        }
        return stackDestiny;
    }

    public static int convertStackCharacterToInt(Stack<Character> characterStack){
        int result = 0;
        Character currentCharacter;
        while (!characterStack.isEmpty()){
            currentCharacter = characterStack.pop();
            result =  Integer.parseInt(result + "" + Character.getNumericValue(currentCharacter));
        }
        return result;
    }

    public static Stack<Character> cloneStack(Stack<Character> stackToClone){
        Stack<Character> backupStack = new Stack<Character>();
        Stack<Character> stackCloned = new Stack<Character>();
        Character currentCharacter;
        while (!stackToClone.isEmpty()){
            currentCharacter = stackToClone.pop();
            stackCloned.push(currentCharacter);
            backupStack.push(currentCharacter);
        }
        stackToClone = copyStackToStack(stackToClone, backupStack);
        return invertStack(stackCloned);
    }



}
