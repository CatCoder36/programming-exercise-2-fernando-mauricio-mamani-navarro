import org.junit.Assert;
import org.junit.Test;

import java.util.Iterator;

public class JULinkedListTest {
    @Test
    public void testSize(){
        JULinkedList<Integer> listIntegers = new JULinkedList<Integer>();
        JULinkedList<String> listStrings = new JULinkedList<String>();

        listIntegers.add(5);
        listIntegers.add(6);
        listIntegers.add(7);
        listIntegers.add(8);
        listIntegers.add(9);
        listIntegers.add(10);

        int valueExpected = listIntegers.size();
        Assert.assertEquals(6,valueExpected);

        listIntegers.remove((Integer)5);
        listIntegers.remove((Integer)8);

        valueExpected = listIntegers.size();
        Assert.assertEquals(4,valueExpected);

        listStrings.add("Text1");
        listStrings.add("Text2");
        listStrings.add("Text3");
        listStrings.add("Text4");
        listStrings.add("Text5");

        valueExpected = listStrings.size();
        Assert.assertEquals(5,valueExpected);

        listStrings.remove(0);
        listStrings.remove(1);
        valueExpected = listStrings.size();
        Assert.assertEquals(3,valueExpected);

    }

    @Test
    public void testIsEmpty(){
        JULinkedList<Integer> listIntegers = new JULinkedList<Integer>();
        JULinkedList<String> listStrings = new JULinkedList<String>();

        boolean valueExpected = listIntegers.isEmpty();
        Assert.assertEquals(true,valueExpected);

        valueExpected = listStrings.isEmpty();
        Assert.assertEquals(true,valueExpected);

        listIntegers.add(1);
        valueExpected = listIntegers.isEmpty();
        Assert.assertEquals(false,valueExpected);

        listStrings.add("Text");
        valueExpected = listStrings.isEmpty();
        Assert.assertEquals(false,valueExpected);

        listIntegers.remove(0);
        valueExpected = listIntegers.isEmpty();
        Assert.assertEquals(true,valueExpected);

        listStrings.remove(0);
        valueExpected = listStrings.isEmpty();
        Assert.assertEquals(true,valueExpected);
    }

    @Test
    public void testIterator(){
        JULinkedList<Integer> listIntegers = new JULinkedList<Integer>();
        JULinkedList<String> listStrings = new JULinkedList<String>();

        listIntegers.add(5);
        listIntegers.add(6);
        listIntegers.add(7);
        listIntegers.add(8);
        listIntegers.add(9);
        listIntegers.add(10);

        listStrings.add("Text1");
        listStrings.add("Text2");
        listStrings.add("Text3");
        listStrings.add("Text4");
        listStrings.add("Text5");

        Iterable<Integer> iterable = new Iterable<Integer>() {
            @Override
            public Iterator<Integer> iterator() {
                return listIntegers.iterator();
            }
        };
        String auxiliaryString = "";
        for(Integer currentElement : iterable){
            auxiliaryString += currentElement+", ";
        }

        Assert.assertEquals(listIntegers.getElements() , auxiliaryString);

        Iterable<String> iterableString = new Iterable<String>() {
            @Override
            public Iterator<String> iterator() {
                return listStrings.iterator();
            }
        };

        auxiliaryString = "";
        for(String currentElement : iterableString){
            auxiliaryString += currentElement+", ";
        }

        Assert.assertEquals(listStrings.getElements(), auxiliaryString);
    }

    @Test
    public void testAdd(){
        JULinkedList<Integer> listIntegers = new JULinkedList<Integer>();
        JULinkedList<String> listStrings = new JULinkedList<String>();

        listIntegers.add(1);
        listIntegers.add(2);
        listIntegers.add(3);
        listIntegers.add(4);
        listIntegers.add(5);
        listIntegers.add(6);

        String resultExpected = "Number elements (6)raiz=Nodo{data=6, next=Nodo{data=5, next=Nodo{data=4, next=Nodo{data=3, next=Nodo{data=2, next=Nodo{data=1, next=null}}}}}}";
        Assert.assertEquals(resultExpected, listIntegers.toString());

        listIntegers.add(7);
        resultExpected = "Number elements (7)raiz=Nodo{data=7, next=Nodo{data=6, next=Nodo{data=5, next=Nodo{data=4, next=Nodo{data=3, next=Nodo{data=2, next=Nodo{data=1, next=null}}}}}}}";
        Assert.assertEquals(resultExpected, listIntegers.toString());

        listStrings.add("Text1");
        listStrings.add("Text2");
        listStrings.add("Text3");
        listStrings.add("Text4");
        listStrings.add("Text5");

       resultExpected = "Number elements (5)raiz=Nodo{data=Text5, next=Nodo{data=Text4, next=Nodo{data=Text3, next=Nodo{data=Text2, next=Nodo{data=Text1, next=null}}}}}";
       Assert.assertEquals(resultExpected, listStrings.toString());

       listStrings.add("Text6");
       resultExpected = "Number elements (6)raiz=Nodo{data=Text6, next=Nodo{data=Text5, next=Nodo{data=Text4, next=Nodo{data=Text3, next=Nodo{data=Text2, next=Nodo{data=Text1, next=null}}}}}}";
       Assert.assertEquals(resultExpected, listStrings.toString());

    }

    @Test
    public void testRemoveWithData(){
        JULinkedList<Integer> listIntegers = new JULinkedList<Integer>();
        JULinkedList<String> listStrings = new JULinkedList<String>();

        listIntegers.add(1);
        listIntegers.add(2);
        listIntegers.add(3);
        listIntegers.add(4);
        listIntegers.add(5);
        listIntegers.add(6);

        listIntegers.remove((Integer)1);
        listIntegers.remove((Integer)2);

        String actualResult = listIntegers.getElements();
        String resultExpected = "6, 5, 4, 3, ";
        Assert.assertEquals(resultExpected,actualResult);

        listIntegers.remove((Integer)5);

        actualResult = listIntegers.getElements();
        resultExpected = "6, 4, 3, ";
        Assert.assertEquals(resultExpected, actualResult);

        listStrings.add("Text1");
        listStrings.add("Text2");
        listStrings.add("Text3");
        listStrings.add("Text4");
        listStrings.add("Text5");

        listStrings.remove("Text1");
        listStrings.remove("Text3");

        actualResult = listStrings.getElements();
        resultExpected = "Text5, Text4, Text2, ";
        Assert.assertEquals(resultExpected, actualResult);
        Assert.assertEquals(true, listStrings.remove("Text5"));
        Assert.assertEquals(false, listStrings.remove("null"));

    }

    @Test
    public void testClear(){
        JULinkedList<Integer> listIntegers = new JULinkedList<Integer>();
        JULinkedList<String> listStrings = new JULinkedList<String>();

        listIntegers.add(1);
        listIntegers.add(2);
        listIntegers.add(3);
        listIntegers.add(4);
        listIntegers.add(5);
        listIntegers.add(6);

        listIntegers.clear();

        Assert.assertEquals(0, listIntegers.size());

        listStrings.add("Text1");
        listStrings.add("Text2");
        listStrings.add("Text3");
        listStrings.add("Text4");
        listStrings.add("Text5");

        listStrings.clear();

        Assert.assertEquals(0, listStrings.size() );
    }

    @Test
    public void testGet(){
        JULinkedList<Integer> listIntegers = new JULinkedList<Integer>();
        JULinkedList<String> listStrings = new JULinkedList<String>();

        listIntegers.add(1);
        listIntegers.add(2);
        listIntegers.add(3);
        listIntegers.add(4);
        listIntegers.add(5);
        listIntegers.add(6);

        Assert.assertEquals((Integer)6, listIntegers.get(0));
        Assert.assertEquals((Integer)5, listIntegers.get(1));
        Assert.assertEquals((Integer)4, listIntegers.get(2));
        Assert.assertEquals((Integer)3, listIntegers.get(3));
        Assert.assertEquals((Integer)2, listIntegers.get(4));

        listStrings.add("Text1");
        listStrings.add("Text2");
        listStrings.add("Text3");
        listStrings.add("Text4");
        listStrings.add("Text5");

        Assert.assertEquals("Text5",listStrings.get(0));
        Assert.assertEquals("Text4",listStrings.get(1));
        Assert.assertEquals("Text3",listStrings.get(2));
        Assert.assertEquals("Text2",listStrings.get(3));
        Assert.assertEquals("Text1",listStrings.get(4));

    }

    @Test
    public void remove(){
        JULinkedList<Integer> listIntegers = new JULinkedList<Integer>();
        JULinkedList<String> listStrings = new JULinkedList<String>();

        listIntegers.add(1);
        listIntegers.add(2);
        listIntegers.add(3);
        listIntegers.add(4);
        listIntegers.add(5);
        listIntegers.add(6);

        listIntegers.remove(0);
        String valueExpected = "5, 4, 3, 2, 1, ";
        String currentValor = listIntegers.getElements();
        Assert.assertEquals(valueExpected, currentValor);

        listIntegers.remove(0);
        listIntegers.remove(1);
        valueExpected = "4, 2, 1, ";
        currentValor = listIntegers.getElements();
        Assert.assertEquals(valueExpected, currentValor);

        listStrings.add("Text1");
        listStrings.add("Text2");
        listStrings.add("Text3");
        listStrings.add("Text4");
        listStrings.add("Text5");

        listStrings.remove(0);
        valueExpected = "Text4, Text3, Text2, Text1, ";
        currentValor = listStrings.getElements();
        Assert.assertEquals(valueExpected, currentValor);

        listStrings.remove(0);
        listStrings.remove(0);

        valueExpected = "Text2, Text1, ";
        currentValor = listStrings.getElements();
        Assert.assertEquals(valueExpected, currentValor);
    }

    @Test
    public void testContains(){
        JULinkedList<Integer> listIntegers = new JULinkedList<Integer>();
        JULinkedList<String> listStrings = new JULinkedList<String>();

        listIntegers.add(1);
        listIntegers.add(2);
        listIntegers.add(3);
        listIntegers.add(4);
        listIntegers.add(5);
        listIntegers.add(6);

        Assert.assertEquals(true, listIntegers.contains((Integer)1));
        Assert.assertEquals(true, listIntegers.contains((Integer)2));
        Assert.assertEquals(true, listIntegers.contains((Integer)3));
        Assert.assertEquals(false, listIntegers.contains((Integer)100));
        Assert.assertEquals(false, listIntegers.contains((Integer)200));

        listStrings.add("Text1");
        listStrings.add("Text2");
        listStrings.add("Text3");
        listStrings.add("Text4");
        listStrings.add("Text5");

        Assert.assertEquals(true, listStrings.contains("Text1"));
        Assert.assertEquals(true, listStrings.contains("Text2"));
        Assert.assertEquals(true, listStrings.contains("Text3"));
        Assert.assertEquals(false, listStrings.contains("Text100"));
        Assert.assertEquals(false, listStrings.contains("Text1000"));
    }

    @Test
    public void testSet(){
        JULinkedList<Integer> listIntegers = new JULinkedList<Integer>();
        JULinkedList<String> listStrings = new JULinkedList<String>();

        listIntegers.add(1);
        listIntegers.add(2);
        listIntegers.add(3);
        listIntegers.add(4);
        listIntegers.add(5);
        listIntegers.add(6);

        listIntegers.set(0,9);

        String actualValue = listIntegers.getElements();
        String expectedValue = "9, 5, 4, 3, 2, 1, ";
        Assert.assertEquals(expectedValue,actualValue);

        listIntegers.set(1, 85);
        listIntegers.set(2, 86);
        listIntegers.set(3, 87);
        listIntegers.set(4, 88);

        actualValue = listIntegers.getElements();
        expectedValue = "9, 85, 86, 87, 88, 1, ";
        Assert.assertEquals(expectedValue, actualValue);

        listStrings.add("Text1");
        listStrings.add("Text2");
        listStrings.add("Text3");
        listStrings.add("Text4");
        listStrings.add("Text5");

        listStrings.set(0, "Hello1");

        actualValue = listStrings.getElements();
        expectedValue = "Hello1, Text4, Text3, Text2, Text1, ";
        Assert.assertEquals(expectedValue, actualValue);

        listStrings.set(1, "Hello2");
        listStrings.set(2, "Hello3");
        listStrings.set(3, "Hello4");
        listStrings.set(4, "Hello5");

        actualValue = listStrings.getElements();
        expectedValue = "Hello1, Hello2, Hello3, Hello4, Hello5, ";
        Assert.assertEquals(expectedValue, actualValue);
    }

    @Test
    public void testIndexOf(){
        JULinkedList<Integer> listIntegers = new JULinkedList<Integer>();
        JULinkedList<String> listStrings = new JULinkedList<String>();

        listIntegers.add(1);
        listIntegers.add(5);
        listIntegers.add(3);
        listIntegers.add(2);
        listIntegers.add(5);
        listIntegers.add(2);

        Assert.assertEquals(0,listIntegers.indexOf(2));
        Assert.assertEquals(1,listIntegers.indexOf(5));
        Assert.assertEquals(3,listIntegers.indexOf(3));
        Assert.assertEquals(-1,listIntegers.indexOf(456));

        listStrings.add("Text1");
        listStrings.add("Text3");
        listStrings.add("Text3");
        listStrings.add("Text5");
        listStrings.add("Text5");

        Assert.assertEquals(0,listStrings.indexOf("Text5"));
        Assert.assertEquals(2,listStrings.indexOf("Text3"));
        Assert.assertEquals(4,listStrings.indexOf("Text1"));
        Assert.assertEquals(-1,listStrings.indexOf("TextNull"));
    }

    @Test
    public void testLastIndexOf(){
        JULinkedList<Integer> listIntegers = new JULinkedList<Integer>();
        JULinkedList<String> listStrings = new JULinkedList<String>();

        listIntegers.add(1);
        listIntegers.add(5);
        listIntegers.add(3);
        listIntegers.add(2);
        listIntegers.add(5);
        listIntegers.add(2);

        Assert.assertEquals(2,listIntegers.lastIndexOf(2));
        Assert.assertEquals(4,listIntegers.lastIndexOf(5));
        Assert.assertEquals(-1,listIntegers.lastIndexOf(000000000));

        listStrings.add("Text1");
        listStrings.add("Text3");
        listStrings.add("Text3");
        listStrings.add("Text5");
        listStrings.add("Text5");

        Assert.assertEquals(1,listStrings.lastIndexOf("Text5"));
        Assert.assertEquals(3,listStrings.lastIndexOf("Text3"));
        Assert.assertEquals(-1,listStrings.lastIndexOf("TextNull"));
    }


}
