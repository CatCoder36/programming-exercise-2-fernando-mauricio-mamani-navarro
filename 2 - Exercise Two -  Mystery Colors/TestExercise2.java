import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class TestExercise2 {
    @Test
    public void testNumberOfDistinctColors(){
        MysteryColorAnalyzerImpl mysteryColorAnalyzer = new MysteryColorAnalyzerImpl();
        ArrayList<Color> colors = new ArrayList<>(List.of(Color.BLUE , Color.BLUE, Color.GREEN, Color.GREEN));
        int quantityExpected = mysteryColorAnalyzer.numberOfDistinctColors(colors);
        Assert.assertEquals(2,quantityExpected);
    }

    @Test
    public void testColorOcurrence(){
        MysteryColorAnalyzerImpl mysteryColorAnalyzer = new MysteryColorAnalyzerImpl();
        ArrayList<Color> colors = new ArrayList<>(List.of(Color.BLUE , Color.BLUE, Color.GREEN, Color.GREEN, Color.RED, Color.GREEN));
        int quantityExpected = mysteryColorAnalyzer.colorOccurrence(colors, Color.GREEN);
        Assert.assertEquals(3,quantityExpected);
    }
}
