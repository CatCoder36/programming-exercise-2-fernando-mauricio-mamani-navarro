import java.util.ArrayList;

/**
 *This class adds the consecutive odd or even numbers of an array.
 */
public class ArraySumAnalyzer {
    public int addingNumberOfFollowedArray(int[] arrayAnalyzed){
        int numberTotalElements = 0;
        int sumElements = 0;
        boolean stop = true;
        do{
            ArrayList<Integer> auxiliaryList = new ArrayList<>();
            for(int currentPosition = 0; currentPosition < arrayAnalyzed.length ; currentPosition++){
                sumElements = arrayAnalyzed[currentPosition];
                boolean typeValueAnalyzed = checkEvenNumber(arrayAnalyzed[currentPosition]);
                boolean numberTypeEvaluated = currentPosition != arrayAnalyzed.length -1 ? checkEvenNumber(arrayAnalyzed[currentPosition + 1]) : !typeValueAnalyzed;
                while(numberTypeEvaluated == typeValueAnalyzed){
                    currentPosition++;
                    int numberAnalyzed = arrayAnalyzed[currentPosition];
                    sumElements += numberAnalyzed;
                    numberTypeEvaluated = currentPosition != arrayAnalyzed.length -1 ? checkEvenNumber(arrayAnalyzed[currentPosition + 1]) : !typeValueAnalyzed;
                }
                auxiliaryList.add(sumElements);

            }
            stop = (auxiliaryList.size() != arrayAnalyzed.length);
            arrayAnalyzed = convertArrayListToArray(auxiliaryList);
        }while (stop);

        numberTotalElements = arrayAnalyzed.length;
        return numberTotalElements;
    }

    /**
     * This method determines if a number is even.
     *
     * @param numberAnalyzed int type, receives the value to be analyzed
     * @return boolean type,  Returned a boolean after verifying whether or not it is an even number
     */
    private boolean checkEvenNumber(int numberAnalyzed){
        boolean isPar;
        isPar = numberAnalyzed % 2 == 0;
        return isPar;
    }

    /**
     * This method convert an ArrayList to Array.
     *
     * @param integerArrayList ArrayList type, receives the ArrayList for the convert.
     * @return Array type, returns an array-
     */
    public int[] convertArrayListToArray(ArrayList<Integer> integerArrayList){
        int[] arrayReturned = new int[integerArrayList.size()];
        for(int actualPosition = 0 ; actualPosition < integerArrayList.size() ; actualPosition++){
            arrayReturned[actualPosition] = integerArrayList.get(actualPosition);
        }
        return arrayReturned;
    }
}
