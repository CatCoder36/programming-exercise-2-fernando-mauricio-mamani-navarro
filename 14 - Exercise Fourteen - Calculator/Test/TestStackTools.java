import org.junit.Assert;
import org.junit.Test;
import Stack.*;
public class TestStackTools {
    @Test
    public void testDeleteElementsToTheStack(){
        Stack<Character> stack1 = new Stack<Character>();
        stack1.push('5');
        stack1.push('4');
        stack1.push('3');
        stack1.push('2');
        stack1.push('1');
        stack1 = StackTools.deleteElementsToTheStack(stack1, 2);
        String result = stack1.getElements();
        String expectedResult = "3, 4, 5, ";

        Assert.assertEquals(expectedResult, result);

        Stack<Character> stack2 = new Stack<Character>();
        stack2.push('5');
        stack2.push('4');
        stack2.push('3');
        stack2.push('2');
        stack2.push('1');

        stack2 = StackTools.deleteElementsToTheStack(stack2, 1);
        result = stack2.getElements();
        expectedResult = "2, 3, 4, 5, ";
    }

    @Test
    public void testMergeStacks(){
        Stack<Character> stack1 = new Stack<Character>();
        stack1.push('1');
        stack1.push('2');
        stack1.push('3');
        stack1.push('4');
        stack1.push('5');

        Stack<Character> stack2 = new Stack<Character>();
        stack2.push('0');
        stack2.push('9');
        stack2.push('8');
        stack2.push('7');
        stack2.push('6');

        stack1 = StackTools.mergeStacks(stack1, stack2);

        String result = stack1.getElements();
        String expected = "0, 9, 8, 7, 6, 5, 4, 3, 2, 1, ";

        Assert.assertEquals(expected, result);

        Stack<Character> stack3 = new Stack<Character>();

        Stack<Character> stack4 = new Stack<Character>();
        stack4.push('0');
        stack4.push('9');
        stack4.push('8');
        stack4.push('7');
        stack4.push('6');

        stack3 = StackTools.mergeStacks(stack3, stack4);
        result = stack3.getElements();
        expected = "0, 9, 8, 7, 6, ";

        Assert.assertEquals(expected, result);
    }

    @Test
    public void testInvertStack(){
        Stack<Character> stack1 = new Stack<Character>();
        stack1.push('1');
        stack1.push('2');
        stack1.push('3');
        stack1.push('4');
        stack1.push('5');

        stack1 = StackTools.invertStack(stack1);
        String result = stack1.getElements();
        String expected = "1, 2, 3, 4, 5, ";

        Assert.assertEquals(expected, result);

        Stack<Character> stack2 = new Stack<Character>();
        stack2.push('0');
        stack2.push('9');
        stack2.push('8');
        stack2.push('7');
        stack2.push('6');

        stack2 = StackTools.invertStack(stack2);

        result = stack2.getElements();
        expected = "0, 9, 8, 7, 6, ";

        Assert.assertEquals(expected, result);
    }

    @Test
    public void copyIntegerToStack(){
        int integerTest = 123456;
        Stack<Character> stack1 = StackTools.copyIntegerToStack(integerTest);
        String result = stack1.getElements();
        String expected = "1, 2, 3, 4, 5, 6, ";

        Assert.assertEquals(expected, result);

        integerTest = 45221;
        Stack<Character> stack2 = StackTools.copyIntegerToStack(integerTest);
        result = stack2.getElements();
        expected = "4, 5, 2, 2, 1, ";

        Assert.assertEquals(expected, result);
    }

    @Test
    public void testConvertStackCharacterToInt(){
        Stack<Character> stack1 = new Stack<Character>();
        stack1.push('1');
        stack1.push('2');
        stack1.push('3');
        stack1.push('4');
        stack1.push('5');

        int result = StackTools.convertStackCharacterToInt(stack1);
        int expected = 54321;

        Assert.assertEquals(expected, result);

        Stack<Character> stack2 = new Stack<Character>();
        stack2.push('1');
        stack2.push('9');
        stack2.push('8');
        stack2.push('7');
        stack2.push('6');

        result = StackTools.convertStackCharacterToInt(stack2);
        expected = 67891;

        Assert.assertEquals(expected, result);
    }

    @Test
    public void testCloneStack(){
        Stack<Character> stack1 = new Stack<Character>();
        stack1.push('1');
        stack1.push('2');
        stack1.push('3');
        stack1.push('4');
        stack1.push('5');

        Stack<Character> stack2 = StackTools.cloneStack(stack1);
        stack1.push('0');

        String result = stack2.getElements();
        String expected = "5, 4, 3, 2, 1, ";

        Assert.assertEquals(expected, result);
    }
}
