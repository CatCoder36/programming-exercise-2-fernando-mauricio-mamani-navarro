/**
 * This interface represents the colors
 */
public enum Color {
    RED, GREEN, BLUE
}
