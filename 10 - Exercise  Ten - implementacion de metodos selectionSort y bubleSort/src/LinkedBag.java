class LinkedBag<T extends Comparable<T> > implements Bag<T> {
    private int size;
    private Node3<T> root;
    public boolean add(T data) {
        Node3<T> node = new Node3<>(data);
        node.setNext(root);
        root = node;
        size++;
        return true;
    }

    public String toString() {
        return "(" + size + ")" + "root=" + root;
    }

    private Node3<T> getPositionInTheList(int position){
        Node3<T> nodeInThePosition = root;
        for(int i = 0; i < position; i++){
            nodeInThePosition = nodeInThePosition.getNext();
        }
        return nodeInThePosition;
    }

    private boolean isHigher(T dataOne , T dataTwo){

        int compared = dataOne.compareTo(dataTwo);
        return compared > 0;
    }
    public void selectionSort() {
       Node3<T> positionEvaluated = root;
       Node3<T> currentNode2;
       Node3<T> minimumNode = root;
       Node3<T> previousMinimum = null;
       Node3<T> auxiliaryNode = null;
       Node3<T> previousCurrentNode = null;
       boolean minimumFound = false;

        for (int indexLap = 0; indexLap < size; indexLap++) {

            for (currentNode2 = positionEvaluated; currentNode2.getNext() != null; currentNode2 = currentNode2.getNext()) {
                if (isHigher(minimumNode.getData(), currentNode2.getNext().getData())) {
                    minimumNode = currentNode2.getNext();
                    previousMinimum = currentNode2;
                    minimumFound = true;
                }
            }
            if(minimumFound){
                if(positionEvaluated.getNext().equals(minimumNode)){
                    positionEvaluated.setNext(minimumNode.getNext());
                    minimumNode.setNext(positionEvaluated);

                }else {
                    auxiliaryNode = positionEvaluated.getNext();
                    positionEvaluated.setNext(minimumNode.getNext());
                    minimumNode.setNext(auxiliaryNode);
                    previousMinimum.setNext(positionEvaluated);
                }
                if (indexLap == 0) {
                    root = minimumNode;
                } else {
                    previousCurrentNode.setNext(minimumNode);
                }
                minimumFound = false;
            }
            previousCurrentNode = getPositionInTheList(indexLap);
            positionEvaluated = previousCurrentNode.getNext();
            minimumNode = positionEvaluated;
        }
    }
    public void bubbleSort() {
        Node3<T> previousNode = null;
        Node3<T> indexNode;
        Node3<T> currentNode;
        Node3<T> nextNode;
        T actualData;
        T nextData;

        for(int i = 0; i < size; i++){
            currentNode = root;
            nextNode = currentNode.getNext();

            for(int indexLaps = 0; indexLaps < size - 1; indexLaps++){
                actualData = currentNode.getData();
                nextData = nextNode.getData();
                if(isHigher(actualData, nextData)){
                    currentNode.setNext(nextNode.getNext());
                    nextNode.setNext(currentNode);
                    if(indexLaps == 0){
                        root = nextNode;
                    }
                    else {
                        previousNode.setNext(nextNode);
                    }
                }
               previousNode = getPositionInTheList(indexLaps);
                indexNode = previousNode.getNext();
                currentNode = indexNode;
                nextNode = currentNode.getNext();
            }
        }

    }

    public String getElements(){
        String elements = "";
        for (Node3<T> currentElement = root; currentElement != null ; currentElement = currentElement.getNext()){
            elements += currentElement.getData()+", ";
        }
        return elements;
    }
}
