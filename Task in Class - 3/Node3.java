public class Node5 {




    private int data;
    private Node5 next;

    public Node5(int data, Node5 next) {
        this.data = data;
        this.next = next;
    }

    public Node5 getNext() {
        return next;
    }
    public void setNext(Node5 next) {
        this.next = next;
    }
    public int getData() {
        return data;
    }

    public Node5(int data) {
        this.data = data;
    }
    public String toString() {
        return "{data=" + data + ", sig->" + next + "}";
    }
}

