import java.util.ArrayList;

/**
 * This class manage the Board logic and properties.
 * @author  Fernando Mauricio Mamamni Navarro
 */
public class Board {
    private final int LENGTH_Y = 10;
    private final int LENGTH_X = 10;
    private String[][] board;
    private ArrayList<Player> players;
    private String cellIcon;

    /**
     * This is the constructor method.
     *
     * @see #fillCells()
     * @see #generateObstacles()
     */
    public Board(){
        board = new String[LENGTH_X][LENGTH_Y];
        cellIcon = ".";
        fillCells();
        generateObstacles();
    }

    /**
     * This method fill cells board
     */
    private void fillCells(){
        for (int numberLine = 0; numberLine < LENGTH_Y ; numberLine++){
            for(int numberRow = 0 ; numberRow < LENGTH_X ; numberRow++){
                board[numberRow][numberLine] = cellIcon;
            }
        }
    }

    /**
     * This method generate the obstacles.
     */
    private void generateObstacles(){
        String obstacleIcon = "X";
        int numberObstacles = 5;
        int randomPositionX;
        int randomPositionY;
        for (int currentObstacle = 0; currentObstacle < numberObstacles; currentObstacle++){
            randomPositionX = (int) (Math.random()*(LENGTH_X-1)+0);
            randomPositionY = (int) (Math.random()*(LENGTH_Y-1)+0);
            board[randomPositionX][randomPositionY] = obstacleIcon;
        }
    }

    /**
     * This method draw the player on the board.
     *
     * @param player Player type, receives the player to draw.
     */
    public void drawPlayer(Player player) {
        board[player.getActualPositionX()][player.getActualPositionY()] = player.getIcon();
    }

    /**
     * This method generate a String with the board elements
     *
     * @return String type, returns a String with the board elements
     */
    public String drawElementsBoard(){
        String boardWithElements = "";
        for (int numberLine = 0; numberLine < LENGTH_Y ; numberLine++){
            for(int numberRow = 0 ; numberRow < LENGTH_X ; numberRow++){
                boardWithElements += board[numberRow][numberLine];
            }
            boardWithElements += "\n";
        }
        return boardWithElements;
    }

    /**
     * This method verify if a cell is available.
     *
     * @param positionInX int type, receives the position in X
     * @param positionInY  int type, receives the position in Y
     * @return boolean type, returns if the cell is available
     */
    public boolean checkAvailableCell(int positionInX, int positionInY){
        boolean isAvailable;
        try{
            isAvailable = board[positionInX][positionInY].equals(cellIcon);
        }catch (Exception e){
            isAvailable = false;
        }
        return isAvailable;
    }

    /**
     * This method clean a Cell
     *
     * @param positionX  int type, receives the position in X
     * @param positionY  int type, receives the position in Y
     */
    public void cleanPosition(int positionX ,int positionY){
        board[positionX][positionY] = cellIcon;
    }

    /**
     * This method verify if  a player is winner
     *
     * @param player, Player type, receives a player to analyze.
     * @return Boolean type, returns if a player is winner.
     */
    public boolean verifyWinner(Player player){
        int positionInX = player.getActualPositionX();
        int positionInY = player.getActualPositionY();
        boolean winner;
        if(positionInX == LENGTH_X-1 && positionInY == LENGTH_Y-1){
            winner = true;
        }
        else {
            winner = false;
        }
        return winner;
    }

    /**
     * Gets the Length y value
     *
     * @return int type, returns the length y value.
     */
    public int getLENGTH_Y() {
        return LENGTH_Y;
    }
    /**
     * Gets the Length x value
     *
     * @return int type, returns the length x value.
     */
    public int getLENGTH_X() {
        return LENGTH_X;
    }

    public String[][] getBoard(){
        return board;
    }




}
