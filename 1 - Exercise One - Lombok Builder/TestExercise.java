import org.junit.Assert;
import org.junit.Test;

public class TestExercise {
    @Test
    public void testGreet(){
        People juan = new People(25,"Juan", "Perez");
        String valueExpected = juan.greet();
        Assert.assertEquals("hello my name is Juan", valueExpected);
    }
    @Test
    public void testGetters(){
        People jorge = new People(15,"Jorge", "Heredia");
        int ageExpected = jorge.getAge();
        Assert.assertEquals(15,ageExpected);
        String nameExpected = jorge.getName();
        Assert.assertEquals("Jorge",nameExpected);
        String lastNameExpected = jorge.getLastName();
        Assert.assertEquals("Heredia", lastNameExpected);
        String greetExpected = jorge.getGREET();
        Assert.assertEquals("hello",greetExpected);
    }
}
