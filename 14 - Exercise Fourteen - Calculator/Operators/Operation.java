package Operators;

import Exceptions.CalcException;
import Exceptions.NumberMajorToTheLimitException;
import Exceptions.OperationOutOfContextException;
import Stack.Stack;
import Stack.StackTools;

/**
 * This class manage the operations in the Calculator.
 */
public class Operation implements Comparable<Operation> {
    private Character symbol;
    protected CalcException calcException;

    /**
     * This is hte constructor method
     *
     * @param symbol, Character type, receives the symbol for the operation;
     */
    public Operation(Character symbol){
        this.symbol = symbol;
    }

    /**
     *This method solves the first operation that it finds.
     * It goes through the list looking for an operation with the class symbol, when it finds it,
     * it performs the operation only once and returns a Stack with the digits of the resulting operation.
     *
     * @see #findFirstNumberInStack(Stack, Character)
     * @see StackTools#deleteElementsToTheStack(Stack, int)
     * @see #executeOperation(int, int)
     * @see StackTools#mergeStacks(Stack, Stack)
     * @see #mergeIntegerWithCharacters(int, char)
     * @param mathExpression Stack<Character> type, receives a Stack with a Mathematical expression analyzed.
     * @return  Stack<Character> type, returns the Stack with the mathematical expression resolved.
     */
    public Stack<Character> executeFirstOperationFound(Stack<Character> mathExpression){
        Stack<Character> resultOperation;
        Stack<Character> resultStack = new Stack<Character>();
        Character currentCharacter;
        int nextValue, previousValue = 0;
        boolean operationRealized = false;
        while (!mathExpression.isEmpty()){
            currentCharacter = mathExpression.pop();
            if(currentCharacter == getSymbol() && !operationRealized) {
                nextValue = findFirstNumberInStack(mathExpression, currentCharacter);
                resultStack = StackTools.deleteElementsToTheStack(resultStack, (int)Math.log10(previousValue)+1);
                resultOperation = executeOperation(previousValue,nextValue);
                resultStack = StackTools.mergeStacks(resultStack, resultOperation);
                operationRealized = true;
            } else {
                previousValue = mergeIntegerWithCharacters(previousValue, currentCharacter);
                resultStack.push(currentCharacter);
            }
        }
        return StackTools.invertStack(resultStack);
    }

    /**
     * This method execute the operation of the class;.
     *
     * @param firstNumber int type.
     * @param secondNumber int type.
     * @return int type, result operation
     */
    protected Stack<Character> executeOperation(int firstNumber, int secondNumber){
        return null;
    }

    /**
     * This method joins a character data type with an int data type.
     * This method receives an int and character and returns the int with the digits together.
     *
     * @param firstNumber int type.
     * @param secondNumber char type.
     * @return int type, returns the digits merged
     */
    protected int mergeIntegerWithCharacters(int firstNumber, char secondNumber){
        int result;
        if(isNumber(secondNumber)){
            result =  Integer.parseInt(firstNumber + "" + Character.getNumericValue(secondNumber));
        }
        else {
            result = 0;
        }
        return result;
    }

    /**
     * This method search the first number in a Mathematical Expression and return.
     * This method traverses a stack containing a mathematical expression, when it collects the first number
     * of the expression it returns it
     *
     * @see #mergeIntegerWithCharacters(int, char)
     * @see OperationOutOfContextException
     * @param stackWithNumber  Stack<Character> type, receives a Stack with a Mathematical expression analyzed.
     * @param currentOperator Character type, receives the current CharacterAnalyzed
     * @return int type, returns first number found.
     */
    protected int findFirstNumberInStack(Stack<Character> stackWithNumber, Character currentOperator){
        int firstNumberFound = 0;
        Character currentCharacter;
        boolean operatorFound = false;
        boolean isTheFirstLap = true;
        while (!operatorFound && !stackWithNumber.isEmpty()){
            currentCharacter = stackWithNumber.pop();
            if(isNumber(currentCharacter)) {
                firstNumberFound = mergeIntegerWithCharacters(firstNumberFound, currentCharacter);
                isTheFirstLap = false;
            }
            else {
                operatorFound = true;
                stackWithNumber.push(currentCharacter);
            }
        }
        if(isTheFirstLap){
            calcException = new OperationOutOfContextException(String.valueOf(currentOperator));
        }


        return firstNumberFound;
    }

    /**
     * This method verify if a Character is number.
     *
     * @param character Character type, value analyzed.
     * @return boolean type, return true if character in a number
     */
    protected boolean isNumber(Character character){
        boolean isANumber;
        int numberAnalyzed = Character.getNumericValue(character);
        isANumber = numberAnalyzed >=0 && numberAnalyzed <=9;
        return isANumber;
    }
    public Character getSymbol(){
        return symbol;
    }

    /**
     * This is the compare To method
     *
     * @param currentOperation the object to be compared.
     * @return int type, returns 0 if the elements are equals. returns 1 if the elements are different.
     */
    @Override
    public int compareTo(Operation currentOperation) {
        return symbol == currentOperation.getSymbol()? 0 : 1;
    }

    /**
     * This method return the CalcException of the class.
     *
     * @return return the CalcException of the class.
     */
    public CalcException getCalcException() {
        return calcException;
    }
}
